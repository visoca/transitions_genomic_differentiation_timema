#!/usr/bin/env perl

# (c) Romain Villoutreix
# romain.villoutreix@gmail.com
# Last modified: 24/03/2016

# Description:
# This script plots the sparse values (PIPs along the genome) resulting from the gemma_sparse_formatting.pl function
# generate barplot for mean PIP value along LG


use warnings;
use strict;
use Getopt::Long;
use File::Basename;
use File::Path qw(make_path);
use File::Spec;
use Statistics::R;
use POSIX qw(strftime);

my $version='1.0.1-2016.03.24';

#Default graphic variables
my $y1=0;
my $y2=0;

#Default outfiles
my $outdir='output';
&author;
my ($infile);
GetOptions(
    'i|I=s'  => \$infile,
    'o|O=s'  => \$outdir,
	'y1=f' => \$y1,
	'y2=f' => \$y2,
    'h|help' => \&usage
);

&usage if (!defined($infile) || !defined($outdir));

#Input file does exist ?
if (! -e $infile){
die ("\nCan't access input file: $infile\n\n");
}

# Output directory does exist ?
if (! -e $outdir){
eval {make_path($outdir)}
or die ("\nError creating output directory: $outdir\n\n");
}

#some specifications of path
$infile=File::Spec->rel2abs($infile);
$outdir=File::Spec->rel2abs($outdir);
my $inbase=basename($infile);
$inbase=~ s/\.txt//g;
my $outbase="$outdir/".basename($infile);
$outbase=~ s/\.txt//g;

print "Necessary files found :) \n\n\  Making graphics ! \n\n";

# R code - put whatever you want in here
# --------------------------------------------------------------------------------------------
   my $R = Statistics::R->new();

$R->run(qq`

setwd("$outdir");

#defining graphic variables

	y1 <- $y1;
	y2 <- $y2;

# opening sparse file

	sparse <- read.table(file="$infile", sep="\t", dec=".", header=TRUE);

#discard unmapped LG

	sparsemapped <- sparse[!is.na(sparse\$CHR),];

#Generate the histogram of mean PIP per LG

	meangammaperLG <- aggregate(sparsemapped[, match("gamma", colnames(sparsemapped))], list(sparsemapped\$CHR), mean);

	pdf(file="$outbase\_mean_PIP_per_LG.pdf");

	if(y1==0){barplot(height=meangammaperLG\$x, names.arg=meangammaperLG\$Group.1, ylab = "Mean PIP", xlab="LG", col ="gray");}
	if(y1!=0){barplot(height=meangammaperLG\$x, names.arg=meangammaperLG\$Group.1, ylab = "Mean PIP", xlab="LG", ylim=c(0,y1), col ="gray");}

	dev.off();

# plot the PIP values along the LG in indexed order

	sparsemappedordered <- sparsemapped[order(sparsemapped\$CHR, sparsemapped\$scaforder, sparsemapped\$scaffold, sparsemapped\$PS),];
	sparsemappedordered\$snpindex <- 1:nrow(sparsemappedordered);
	sparsemappedordered\$colour <- "NA";
	sparsemappedordered\$colour[sparsemappedordered\$CHR%%2==0] <- "red";
	sparsemappedordered\$colour[sparsemappedordered\$CHR%%2!=0] <- "yellow";

	jpeg(filename="$outbase\_PIP_along_LG.jpg", width=21, height=10, units="cm", pointsize=12, res=600);

	if(y2==0){plot(x=sparsemappedordered\$snpindex, y=sparsemappedordered\$gamma, col=c(sparsemappedordered\$colour), xlab="Genomic Position Index", ylab="PIP", pch=16, main="");}
	if(y2!=0){plot(x=sparsemappedordered\$snpindex, y=sparsemappedordered\$gamma, col=c(sparsemappedordered\$colour), xlab="Genomic Position Index", ylab="PIP", ylim=c(0,y2), pch=16, main="");}

	dev.off();`);

$R->stop();

print " Done!  Graphs saved in $outdir \n\n  Bye \n\n";

# --------------------------------------------------------------------------------------------#


# ==============================================================================
# ==============================================================================
# ============================== SUBROUTINES ===================================
# ==============================================================================
# ==============================================================================

# Show copyright
# ==============================================================================
sub author{
    print "\n";
    print "#########################################\n";
    print "  ".basename($0)."\n";
	print "  version $version\n";
    print "  (c) Romain Villoutreix            \n";
    print "  romain.villoutreix\@gmail.com      \n";
    print "#########################################\n";
	print "\n";
}
# ==============================================================================

# Show usage
# ==============================================================================
sub usage{
    print "\n";
	print "  Usage:\n";
    print "    ".basename($0)."\n";
	print "      -i <input file (sparse file output from gemma_sparse_formatting.pl)>\n";
	print "      -o <output directory - optional defaults to output/>\n";
	print "      -y1 <max y display value for mean PIP per LG graphic / default = automatic value>\n";
	print "      -y2 <max y display value for PIP along LG graphic / default = automatic value>\n";
	print "      -h <show this help>\n";
    print "\n";
    exit;
}
# ==============================================================================
