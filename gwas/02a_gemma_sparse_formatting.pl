#!/usr/bin/env perl

# (c) Romain Viloutreix
# romain.villoutreix@gmail.com

# Description: Given a Gemma sparse matrix file returns file with chr and PS position filled
# It also extract scaffold and scaffold order values 

use warnings;
use Getopt::Long;
use File::Basename;
use LWP::Simple;
use List::Util qw(sum min);

my $version='2016-03-15';

&author;
my ($infile, $outfile);

GetOptions(
    'i|I=s' => \$infile, # sparse matrix file output from gemma_summary.pl (8 columns: CHR	RS	PS	alpha	beta	gamma	abs.beta.g	dir.beta.g)
	'o|O=s' => \$outfile,
    'h|help'=> \&usage
) or print ("\nERROR: Option/s not recognized (see above)\n") and &usage;


&usage if (!defined($infile));

# -----------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------
# -----------------------------------------------------------------------------------------------


print STDERR "\n\n";

print "Necessary files found :) \n\n\  processing sparse file ! \n\n";

#Opening of the input and output files
my @fields=();
my @subfield1=();
my @subfield2=();
my $lg=();
my $scaffold=();
my $scaforder=();
my $position=();


open (OFH, ">$outfile") or die "\nCan't open $outfile file\n\n";
print OFH "CHR\tRS\tPS\talpha\tbeta\tgamma\tabs.beta.g\tdir.beta.g\tscaffold\tscaforder\n";

open (IFH, $infile) or die "\nCan't open $infile file\n\n";
	while (<IFH>){
	#skip the first line
	next if $. == 1;
	chomp $_;
	@fields = split("\t",$_);
	@subfield1 = split("-", $fields[1]);
	@subfield2 = split("_", $subfield1[0]);
	$lg = substr $subfield2[0], 2;
	$scaffold = substr $subfield2[2], 4;
	$scaforder = substr $subfield2[1], 3;
	$position = $subfield1[1];
	
	print OFH "$lg\t$subfield1[0]-$position\t$position\t$fields[3]\t$fields[4]\t$fields[5]\t$fields[6]\t$fields[7]\t$scaffold\t$scaforder\n";


	}

#closing the files
close(IFH);
close(OFH);


print "Bye ! \n\n";


# ================================================================================================
# Show copyright
# ==============================================================================
sub author{
    print "\n";
    print "#########################################\n";
    print "  ".basename($0)."\n";
	print "  version $version     \n";
    print "  (c) Romain Villoutreix             \n";
    print "  romain.villoutreix\@gmail.com      \n";
    print "#########################################\n";
	print "\n";
}
# ==============================================================================

# Show usage
# ==============================================================================
sub usage{
    print "\n";
	print "  Usage:\n";
    print "    ".basename($0)."\n";
	print "      -i <sparse effects file from gemma_summary.pl>\n";
	print "      -o <output file>\n";
    print "\n";
    exit;
}
# ==============================================================================
