#!/usr/bin/env perl

# (c) Stuart Dennis modified by Romain Villoutreix
# stuart.dennis@sheffield.ac.uk
# Last modified: 03/03/2016

# Description:
# This script summarizes parameters from multiple chain - GEMMA analyses - but realize a summary for each chain
# currently only for bslmm
# execute this sript from the base folder for each analysis - this folder must contain
# a sub folder entitled "output" which in turn contains the "hyp.txt" and "para.txt" files
# generated using the script gemma_bslmm.pl


use warnings;
use strict;
use Getopt::Long;
use File::Basename;
use File::Path qw(make_path);
use File::Spec;
use Statistics::R;
use POSIX qw(strftime);

my $version='1.1.0-2016.04.21';

#Default outfiles
my $outdir='output';
&author;
my ($infile);
GetOptions(
    'i|I=s'  => \$infile,
    'o|O=s'  => \$outdir,
    'h|help' => \&usage
);

&usage if (!defined($infile) || !defined($outdir));

#Input file
if (! -e $infile){
die ("\nCan't access input file: $infile\n\n");
}
$infile=File::Spec->rel2abs($infile);
$outdir=File::Spec->rel2abs($outdir);
my $inbase=basename($infile);
$inbase=~ s/\.geno//g;
$inbase=~ s/\.bbgeno//g;
my $outbase="$outdir/".basename($infile);
$outbase=~ s/\.geno//g;

print "Necessary files found :) \n\n\  Performing calculations and tabulating results \n\n";

# R code - put whatever you want in here
# --------------------------------------------------------------------------------------------
   my $R = Statistics::R->new();

$R->run(qq`

setwd("$outdir")
	gemma.files <- list.files(pattern="$inbase");
			hyp.files<-grep(".hyp.txt",gemma.files,value=TRUE);
			para.files<-grep(".param.txt",gemma.files,value=TRUE);
						for (i in seq_along(hyp.files)) {
				if (i == 1) {
					hyp.dat <- read.table(hyp.files[i], header=TRUE);
					hyp.dat\$chain <- i;
				} else {
					dat <- read.table(hyp.files[i], header=TRUE);
					dat\$chain <- i;
					hyp.dat <- rbind(hyp.dat,dat);
				}
			}


#Generate the table for median and 95% EPTI estimates for h, PVE, rho, PGE
# 50% quantile == median values, 0.025 and 0.975 are lower and upper 95%ETPI

	 h     <- quantile(hyp.dat\$h, probs = c(0.5, 0.025, 0.975))
	 PVE   <- quantile(hyp.dat\$pve, probs = c(0.5, 0.025, 0.975))
	 rho   <- quantile(hyp.dat\$rho, probs = c(0.5, 0.025, 0.975))
	 PGE   <- quantile(hyp.dat\$pge, probs = c(0.5, 0.025, 0.975))
	 n_gam <- quantile(hyp.dat\$n_gamma, probs = c(0.5,0.025, 0.975))

	 table <- rbind(h,rho,PVE,PGE,n_gam)
	 colnames(table) <- c("median", "low95EPTI", "upp95EPTI")

# save table:
write.table(table,"$outbase\_summary_table.csv",quote=FALSE,sep=",",row.names=TRUE,col.names=TRUE)


#Generate a table for median and 95% EPTI estimates for h, PVE, roh, PGE per chain !

for(chain in c(unique(hyp.dat\$chain))){

	h     <- quantile(hyp.dat\$h[hyp.dat\$chain==chain], probs = c(0.5, 0.025, 0.975))
	PVE   <- quantile(hyp.dat\$pve[hyp.dat\$chain==chain], probs = c(0.5, 0.025, 0.975))
	rho   <- quantile(hyp.dat\$rho[hyp.dat\$chain==chain], probs = c(0.5, 0.025, 0.975))
    PGE   <- quantile(hyp.dat\$pge[hyp.dat\$chain==chain], probs = c(0.5, 0.025, 0.975))
	n_gam <- quantile(hyp.dat\$n_gamma[hyp.dat\$chain==chain], probs = c(0.5,0.025, 0.975))

	table <- rbind(h,rho,PVE,PGE,n_gam)
	colnames(table) <- c("median", "low95EPTI", "upp95EPTI")

	# save table:
	write.table(table,paste("$outbase\_summary_table_chain_", chain, ".csv", sep=""),quote=FALSE,sep=",",row.names=TRUE,col.names=TRUE)

}


#Generate a graphic with the density distributions of all hyper parameters:
pdf(file="$outbase\_hyp_param_distribution.pdf")

	mat <- matrix(c(1:6), ncol=2, nrow=3, byrow=TRUE);
	layout(mat);

	dh <- density(hyp.dat\$h);
	plot(dh, main="h");

	dpve <- density(hyp.dat\$pve);
	plot(dpve, main="PVE");

	drho <- density(hyp.dat\$rho);
	plot(drho, main="rho");

	dpge <- density(hyp.dat\$pge);
	plot(dpge, main="PGE");

	dn_gamma <- density(hyp.dat\$n_gamma);
	plot(dn_gamma, main="n_gam");

	dev.off();


#Generate distribution for each chain
	for (chain in c(unique(hyp.dat\$chain))){
	pdf(file=paste("$outbase\_hyp_param_distribution_chain_", chain, ".pdf", sep=""));

	mat <- matrix(c(1:6), ncol=2, nrow=3, byrow=TRUE);
	layout(mat);

	dh <- density(hyp.dat\$h[hyp.dat\$chain==chain]);
	plot(dh, main="h");

	dpve <- density(hyp.dat\$pve[hyp.dat\$chain==chain]);
	plot(dpve, main="PVE");

	drho <- density(hyp.dat\$rho[hyp.dat\$chain==chain]);
	plot(drho, main="rho");

	dpge <- density(hyp.dat\$pge[hyp.dat\$chain==chain]);
	plot(dpge, main="PGE");

	dn_gamma <- density(hyp.dat\$n_gamma[hyp.dat\$chain==chain]);
	plot(dn_gamma, main="n_gam");

	dev.off();
	}



	#h
	png(filename="$outbase\_chains_trace_h.png", width=2400, height=400, res=100);
	plot(seq(length(hyp.dat\$h)), hyp.dat\$h, type="l", xlab="MCMC chains", xaxt='n', ylab="h");
	axis(side=1, labels=FALSE);
	for (k in 2:max(hyp.dat\$chain)) {abline (v=((length(hyp.dat\$h)/max(hyp.dat\$chain))*(k-1)), lty=3, lwd=2, col="grey")};
	abline(h=median(hyp.dat\$h));
	abline(h=quantile(hyp.dat\$h, c(0.025,0.975)), col="red", lty=3);
	dev.off();

	# PVE
	png(filename="$outbase\_chains_trace_PVE.png", width=2400, height=400, res=100);
	plot(seq(length(hyp.dat\$pve)), hyp.dat\$pve, type="l", xlab="MCMC chains", xaxt='n', ylab="PVE");
	axis(side=1, labels=FALSE);
	for (k in 2:max(hyp.dat\$chain)) {abline (v=((length(hyp.dat\$pve)/max(hyp.dat\$chain))*(k-1)), lty=3, lwd=2, col="grey")};
	abline(h=median(hyp.dat\$pve));
	abline(h=quantile(hyp.dat\$pve, c(0.025,0.975)), col="red", lty=3);
	dev.off();

	# rho
	png(filename="$outbase\_chains_trace_rho.png", width=2400, height=400, res=100);
	plot(seq(length(hyp.dat\$rho)), hyp.dat\$rho, type="l", xlab="MCMC chains", xaxt='n', ylab="rho");
	axis(side=1, labels=FALSE);
	for (k in 2:max(hyp.dat\$chain)) {abline (v=((length(hyp.dat\$rho)/max(hyp.dat\$chain))*(k-1)), lty=3, lwd=2, col="grey")};
	abline(h=median(hyp.dat\$rho));
	abline(h=quantile(hyp.dat\$rho, c(0.025,0.975)), col="red", lty=3);
	dev.off();

	# PGE
	png(filename="$outbase\_chains_trace_pge.png", width=2400, height=400, res=100);
	plot(seq(length(hyp.dat\$pge)), hyp.dat\$pge, type="l", xlab="MCMC chains", xaxt='n', ylab="PGE");
	axis(side=1, labels=FALSE);
	for (k in 2:max(hyp.dat\$chain)) {abline (v=((length(hyp.dat\$pge)/max(hyp.dat\$chain))*(k-1)), lty=3, lwd=2, col="grey")};
	abline(h=median(hyp.dat\$pge));
	abline(h=quantile(hyp.dat\$pge, c(0.025,0.975)), col="red", lty=3);
	dev.off();

	# n_gamma
	png(filename="$outbase\_chains_trace_ngam.png", width=2400, height=400, res=100);
	plot(seq(length(hyp.dat\$n_gamma)), hyp.dat\$n_gamma, type="l", xlab="MCMC chains", xaxt='n', ylab="n_gam");
	axis(side=1, labels=FALSE);
	for (k in 2:max(hyp.dat\$chain)) {abline (v=((length(hyp.dat\$n_gamma)/max(hyp.dat\$chain))*(k-1)), lty=3, lwd=2, col="grey")};
	abline(h=median(hyp.dat\$n_gamma));
	abline(h=quantile(hyp.dat\$n_gamma, c(0.025,0.975)), col="red", lty=3);
	dev.off();

    #cleanup
    rm(list=ls(pattern="hyp"));


			#get SNPs with individual effects
			for (i in seq_along(para.files)) {
			  if (i == 1) {
			      para.dat <- read.table(para.files[i], header=TRUE)
			  } else {
				  dat <- read.table(para.files[i], header=TRUE)
				  para.dat <- cbind(para.dat, dat[,4:7])
			  }
			}

# calculate mean alpha, beta, and gamma across all chains
		para.mean <- data.frame(cbind(para.dat[,1:3], apply(para.dat[,seq(5,dim(para.dat)[2],4)], MARGIN=1, FUN=mean), apply(para.dat[,seq(6,dim(para.dat)[2],4)], MARGIN=1, FUN=mean), apply(para.dat[,seq(7,dim(para.dat)[2],4)], MARGIN=1, FUN=mean)));

		colnames(para.mean) <- c("CHR","RS","PS","alpha","beta","gamma");
		para.mean\$abs.beta.g  <- abs(para.mean\$beta * para.mean\$gamma) # calculate absolute beta x gamma
		para.mean\$dir.beta.g  <- para.mean\$beta * para.mean\$gamma # calculate directional beta x gamma

		#cleanup
		rm(list=ls(pattern="dat"))


write.table(para.mean, file="$outbase\_sparse_effects_all_SNPs_except_MAFcutoff.txt", quote=FALSE, row.names=FALSE, sep="\t")`);
$R->stop();

print " Done!  Output files saved in $outdir \n\n  Bye \n\n";

# --------------------------------------------------------------------------------------------#


# ==============================================================================
# ==============================================================================
# ============================== SUBROUTINES ===================================
# ==============================================================================
# ==============================================================================

# Show copyright
# ==============================================================================
sub author{
    print "\n";
    print "#########################################\n";
    print "  ".basename($0)."\n";
	print "  version $version\n";
    print "  (c) Stuart Dennis modified by Romain Villoutreix            \n";
    print "  stuart.dennis\@sheffield.ac.uk      \n";
    print "#########################################\n";
	print "\n";
}
# ==============================================================================

# Show usage
# ==============================================================================
sub usage{
    print "\n";
	print "  Usage:\n";
    print "    ".basename($0)."\n";
	print "      -i <input file (.geno file)>\n";
	print "      -o <output directory - optional defaults to output/>\n";
	print "      -h <show this help>\n";
    print "\n";
    exit;
}
# ==============================================================================
