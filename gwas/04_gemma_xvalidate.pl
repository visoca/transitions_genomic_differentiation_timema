#! /usr/bin/env perl

# (c) Stuart Dennis
# stuart.dennis@sheffield.ac.uk
# Last modified: 18/02/2015 16:35:17

# Description:
# This script  will calculate area under ROC ( ) - Known as AUC for genomic prediction,
# Execute this script from the same base directory for each analysis

# Requirements:
# 1. designed to work with output from gemma_permutations.pl (and subsequent prediction runs)
# i.e. <prefix>.prdt.txt files
# 2. directory structure (for no alterations)  >> permutations/perm[*]/rep[*]/output/
# 3. patience
#
# usage:   gemma_xvalidate.pl -i <test.pheno> -type q


use warnings;
use strict;
use Getopt::Long;
use File::Basename;
use File::Path qw(make_path);
use File::Spec;
use Statistics::R;
use POSIX qw(strftime);

my $date=strftime("%Y%m%d%-H%M%S", localtime);

my $version='1.0.1-2015.02.26';

# default arguments
my $outdir='permutations';
my $train=90;
my $nruns=10;
my $perms=10;
my $type='';

&author;
my ($pheno);
GetOptions(
    'i|I=s'  => \$pheno,
    'type=s'  => \$type,
	'n|N=i'  => \$nruns,
	'perms=i' => \$perms,
	'train=i' => \$train,
	'h|help' => \&usage
);

&usage if (!defined($pheno) || !defined($type));

#Input file
if (! -e $pheno){
die ("\nCan't find phenotype file: \n  Unable to continue, Quitting \:\( \n\n");
}
else{
print "Phenotype file $pheno found...\n\nGathering predictions...\n\n";
}
my $outbase=basename($pheno);

my $nreps=100/(100-$train);

#----------------------------------------
#generate predictions matrices for R
# get mean prediction value of each set of $n MCMC chains
system("for xx in {1..$perms} ; do for j in {1..$nreps} ; do cd permutations/perm\${xx}/rep\${j}/output/ ; paste *prdt* | sed 's/NA/7777777777/g' | awk '{sum=0; for(i=1; i<=NF; i++){sum+=\$i}; sum/=NF; print sum}' | sed 's/7777777777/NA/g' > predictions_p\${xx}_r\${j}.txt ; cd ../../../../ ; done ; done");


#create  matrix of orignal phenotype + $nreps .  Omit rows where phenotype (original) unknown
#
system ("for xx in {1..$perms} ; do cd permutations/perm\${xx} ; paste ../../$pheno rep*/output/predictions* | awk '{if (\$1 != \"NA\") print \$0}' > predictions_p\${xx}.txt ; cd ../.. ; done");

#get PVE and PGE from Gemma summary table from full model
my $sumfile=<output/*summary*>;

my $PVE = `grep PVE $sumfile | cut -d, -f2`;
my $roundedPVE = sprintf "%.3f" , $PVE;

my $PGE = `grep PGE $sumfile | cut -d, -f2`;
my $roundedPGE = sprintf "%.3f" , $PGE;

my $RsqMax = $PVE * $PGE;
my $rounded = sprintf "%.3f" , $RsqMax;

print "Found Gemma summary files.  Mean PVE = $roundedPVE \n\n";


#
#synthesize permutation sets of predictions (R code)
#make sure we have $nruns, $nreps, $perms

if ($type eq "q"){
	    my $R = Statistics::R->new();
		     $R->run(qq`
			     x<-vector('list',$perms)
				for (i in 1:$perms){x[[i]]<-read.delim(paste("permutations/perm",i,"/predictions_p",i,".txt",sep=""), header=FALSE)}
				wrk<-x[[1]][,-1]
				for (i in 2:$perms){cbind(wrk,x[[i]][,-1])->wrk}
				obs<-x[[1]][,1]
				pred<-rowMeans(wrk,na.rm=TRUE)
				model<-lm(obs~pred)

pdf(paste("predictions_r2_p",$perms,"_r",$nreps,".pdf",sep=""),width=6,height=5)
par(mar=c(5.5,5.5,5.5,0.5))
grid<-expand.grid(pred=seq(-1,max(obs),length.out=200))
grid<-data.frame(grid,fit=predict(model,grid,type="response"))
plot(pred~obs,xlim=c(range(obs)),
main=paste("cross-validation prediction accuracy = ",round(sqrt(summary(model)\$adj.r.square),3),sep=""),
ylab="predicted phenotype", xlab="observed phenotype",cex.lab=1.6)
points(grid\$fit~grid\$pred,type="l",lty=2,col=2)
mtext (paste("PVE = ",$roundedPVE," : PGE = ",$roundedPGE,sep=""))
capture.output(summary(model),file=paste("predictions_r2_p",$perms,"_r",$nreps,".txt",sep=""))
capture.output(cor.test(obs,pred),file=paste("cor_predictions_r2_p",$perms,"_r",$nreps,".txt",sep=""))
dev.off()`);
$R->stop();
print "\tOutput figure:   predictions_r2_p$perms\_r$nreps\.pdf \n\n";
print "\tOutput summary:  predictions_r2_p$perms\_r$nreps\.txt \n\n";
print "\tOutput summary:  cor_predictions_r2_p$perms\_r$nreps\.txt \n\n";
}
elsif ($type eq "prop"){
	my $R = Statistics::R->new();
 $R->run(qq`
		x<-vector('list',$perms)
		for (i in 1:$perms){x[[i]]<-read.delim(paste("permutations/perm",i,"/predictions_p",i,".txt",sep=""), header=FALSE)}
		wrk<-x[[1]][,-1]
		for (i in 2:$perms){cbind(wrk,x[[i]][,-1])->wrk}
		obs<-x[[1]][,1]
		pred<-rowMeans(wrk,na.rm=TRUE)
		y<-log((obs/(1-obs))+0.0001)#logit transform  (100 for percent 1 for proportion)
		model<-lm(y~pred)


pdf(paste("predictions_r2_prop_p",$perms,"_r",$nreps,".pdf",sep=""),width=6,height=5)
par(mar=c(5.5,5.5,5.5,0.5))
grid<-expand.grid(pred=seq(0,max(obs),length.out=100))
grid<-data.frame(grid,fit=predict(model,grid,type="response"))
grid\$props<-1/(1+1/exp(grid\$fit))
plot(pred~obs,xlim=c(0,1),ylim=c(0,1),
main=paste("cross-validation prediction accuracy Rsq= ",round((summary(model)\$adj.r.square),3),sep=""),
ylab="predicted phenotype", xlab="observed phenotype",cex.lab=1.6)
points(grid\$props~grid\$pred,type="l",lty=2,col=2)
mtext (paste("PVE = ",$roundedPVE," : PGE = ",$roundedPGE,sep=""))
dev.off()
write.csv(grid,file="debug_pred_values.csv",row.names=FALSE, quote=FALSE)
write.csv(cbind(obs,pred),file="debug_values.csv",row.names=FALSE, quote=FALSE)
capture.output(cor.test(y,pred),file=paste("cor_predictions_r2_prop_p",$perms,"_r",$nreps,".txt",sep=""))
capture.output(summary(model),file=paste("predictions_r2_prop_p",$perms,"_r",$nreps,".txt",sep=""))`);


	 $R->stop();
print "\tOutput figure:   predictions_r2_prop_p$perms\_r$nreps\.pdf \n\n";
print "\tOutput summary:  predictions_r2_prop_p$perms\_r$nreps\.txt \n\n";
print "\tOutput summary:  cor_predictions_r2_prop_p$perms\_r$nreps\.txt \n\n";
}
elsif ($type eq "pc"){
	my $R = Statistics::R->new();
	 $R->run(qq`
	         x<-vector('list',$perms)
		        for (i in 1:$perms){x[[i]]<-read.delim(paste("permutations/perm",i,"/predictions_p",i,".txt",sep=""), header=FALSE)}
				wrk<-x[[1]][,-1]
				for (i in 2:$perms){cbind(wrk,x[[i]][,-1])->wrk}
				obs<-x[[1]][,1]
				pred<-rowMeans(wrk,na.rm=TRUE)
				y<-log((obs/(100-obs))+0.0001)#logit transform  (100 for percent 1 for proportion)
				model<-lm(y~pred)

pdf(paste("predictions_r2_pc_p",$perms,"_r",$nreps,".pdf",sep=""),width=6,height=5)
par(mar=c(5.5,5.5,5.5,0.5))
grid<-expand.grid(pred=seq(0,max(obs),length.out=100))
grid<-data.frame(grid,fit=predict(model,grid,type="response"))
grid\$pc<-(1/(1+1/exp(grid\$fit)))*100
plot(pred~obs,xlim=c(range(obs)),
main=paste("cross-validation prediction accuracy = ",round((summary(model)\$adj.r.square),3),sep=""),
ylab="predicted phenotype", xlab="observed phenotype",cex.lab=1.6)
points(grid\$pc~grid\$pred,type="l",lty=2,col=2)
mtext (paste("PVE = ",$roundedPVE," : PGE = ",$roundedPGE,sep=""))
capture.output(summary(model),file=paste("predictions_r2_pc_p",$perms,"_r",$nreps,".txt",sep=""))
dev.off()`);
	$R->stop();
print "\tOutput figure:   predictions_r2_pc_p$perms\_r$nreps\.pdf \n\n";
print "\tOutput summary:  predictions_r2_pc_p$perms\_r$nreps\.txt \n\n";

}

#binary
elsif ($type eq "bin"){
my $R = Statistics::R->new();
	$R->run(qq`

		require(ROCR)
		x<-vector('list',$perms)
		for (i in 1:$perms){x[[i]]<-read.delim(paste("permutations/perm",i,"/predictions_p",i,".txt",sep=""), header=FALSE)}

#replaced with simpler code that produces a single boxplot for all permutations
#auc<-matrix(NA,nrow=$nreps,ncol=$perms)
#
#		for(ex in 1:$perms){
#			for(i in 1:$nreps){
#				j<-which(is.na(x[[ex]][,(i+1)])==FALSE)
#				obs<-x[[ex]][j,1]
#				est<-x[[ex]][j,(i+1)]
#				pred<-prediction(est,obs)
#				stats_auc<-performance(pred,'auc')
#				auc[i,ex]<-as.numeric(stats_auc\@y.values)
#					}
#				}

xx<-vector('list',$perms)
        for (i in 1:$perms){xx[[i]]<-cbind(obs=x[[i]][,1],est=rowMeans(x[[i]][,-1],na.rm=TRUE))}
		        auc<-matrix(NA,nrow=$perms,ncol=1)

        for(ex in 1:$perms){
			j<-which(is.na(xx[[ex]][,2])==FALSE)
			obs<-xx[[ex]][j,1]
			est<-xx[[ex]][j,2]
			pred<-prediction(est,obs)
			stats_auc<-performance(pred,'auc')
			auc[ex,1]<-as.numeric(stats_auc\@y.values)
			}



		pdf(paste("predictions_auc_p",$perms,"_r",$nreps,".pdf",sep=""),width=6,height=5)
		par(mar=c(5.5,5.5,5.5,0.5))
		boxplot(auc,col="grey",ylim=c(0,1),axes=FALSE,
		main=paste("cross-validation prediction accuracy = ",round(mean(auc),3),sep=""),
		ylab="area under the curve (auc)", xlab="",cex.lab=1.6)
		#axis(1,at=seq(1:$perms),labels=c(1:$perms),cex.axis=1.6)
		axis(2,at=seq(0,1,0.2),cex.axis=1.1)
		box()
		abline(h=0.5,lty=3,lwd=2,col=2)
		mtext (paste("PVE = ",$roundedPVE," : PGE = ",$roundedPGE,sep=""))
		capture.output(stats_auc,file=paste("predictions_auc_p",$perms,"_r",$nreps,".txt",sep=""))
		dev.off()`);
$R->stop();
print "\tOutput figure:   predictions_auc_p$perms\_r$nreps\.pdf \n\n";
print "\tOutput summary:  predictions_auc_p$perms\_r$nreps\.txt \n\n";
}


# --------------------------------------------------------------------------------------------



# ==============================================================================
# ==============================================================================
# ============================== SUBROUTINES ===================================
# ==============================================================================
# ==============================================================================

# Show copyright
# ==============================================================================
sub author{
    print "\n";
    print "#########################################\n";
    print "  ".basename($0)."\n";
	print "  version $version\n";
    print "  (c) Stuart Dennis             \n";
    print "  stuart.dennis\@sheffield.ac.uk      \n";
    print "#########################################\n";
	print "\n";
}
# ==============================================================================

# Show usage
# ==============================================================================
sub usage{
    print "\n";
	print "  Usage:\n";
    print "    ".basename($0)."\n";
	print "      -i <input file (.pheno file used for permutations)>\n";
	print "      -type <type of trait:\n\tq = quantitative\n\tprop = proportion\n\tpc = percent\n\tbin = binary>\n\n";
	print "      -h <show this help>\n";
    print "\n";
    exit;
}
# ==============================================================================
