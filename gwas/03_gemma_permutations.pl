#! /usr/bin/env perl

# (c) Stuart Dennis
# stuart.dennis@sheffield.ac.uk
# Last modified: 09/02/2015 18:35:17

# Description:
# This script creates subdirectory structure for genomic prediction,
# populates with training sets as specified by the user,
# and generates submission scripts for Iceberg - the compute cluster at The University of Sheffield.
# The script can be modified to run on other compute clusters but is not supported.
# Requirements:
# 1. a .pheno file containing phenotype information
# 2. a genotype file (.geno) - must include 2-allele columns (eg A C)
# 3. patience


use warnings;
use strict;
use Getopt::Long;
use File::Basename;
use File::Path qw(make_path);
use File::Spec;
use Statistics::R;
use POSIX qw(strftime);

my $date=strftime("%Y%m%d%-H%M%S", localtime);

my $version='1.0.3-2015.02.09';

# default arguments
my $outdir='output';
my $perms=10;
my $train=90;
my $bslmm=3;
my $nruns=10;
my $queue='iceberg';

&author;
my ($infile);
GetOptions(
    'i|I=s'  => \$infile,
    'o|O=s'  => \$outdir,
    'n|N=i'  => \$nruns,
	'perms=i' => \$perms,
	'train=i' => \$train,
    'bslmm=i' => \$bslmm,
	'q|Q=s'  => \$queue,
	'h|help' => \&usage
);

&usage if (!defined($infile) || !defined($outdir)|| !defined($train));

#Input file
if (! -e $infile){
die ("\nCan't find input file: $infile\n\n");
}

my $pheno=<*.pheno>;
if (! -e $pheno){
die ("\nCan't find phenotype file: \n  Unable to continue, Quitting \:\( \n\n");
}
else{
print "Phenotype file $pheno found\n\n";
}

my $geno=basename($infile);
$geno=File::Spec->rel2abs($geno);

if (! -e $geno){
print " Genotype file not found.  You must supply a genotype file \n\n"
}
else {
print "Genotype file $geno found\n\n";
}

#----------------------------------------
#generate directory substructure for predictions
#

my $nreps=100/(100-$train);

system ("mkdir -p permutations/perm{1..$perms}/rep{1..$nreps}");

#construct test sets for predictions (R code)

my $R = Statistics::R->new();

	 $R->run(qq`
		permuteme<-function(dataset){
				keep<-scan(dataset)
					pheno<-c(1:length(keep))
							for (z in 1:$perms){
							perm<-sample(pheno)
							perm[perm==NA]<-7777777
							permute<-data.frame(sapply(1:$nreps,function(x){assign(paste("rnd",x,sep=""), keep)}))
							i=1 ; j = i+((length(keep)/$nreps)-1) ; k = 1
							while(k<$nreps+1){permute[perm[i:j],k]<-NA ; i = i+(length(keep)/$nreps) ; j = i+((length(keep)/$nreps)-1) ; k = k+1 }
							perm[perm==7777777]<-NA
							write.table(permute, file=paste("permutations/",dataset,"_permute_",z,".dsv",sep=""),row.names=FALSE,col.names=FALSE, quote=FALSE)
							}}
		permuteme(dir(pattern="pheno"))`);
	 $R->stop();
my $qpheno=<*.pheno>;
system("parallel \"cut -d' ' -f{2} permutations/$qpheno\_permute_{1}.dsv  > permutations/perm{1}/rep{2}/$qpheno\_permutation{2}.pheno\" ::: {1..$perms} ::: {1..$nreps}");

#insert symlinks to genotype file...
my $qgeno=<*.geno>;
system("parallel \"cd permutations/perm{1}/rep{2} ; ln -s $geno $qgeno ; cd ../../../ \" ::: {1..$perms} ::: {1..$nreps}");

#generate gemma submission scripts for predictions

system("parallel \"cd permutations/perm{1}/rep{2} ; gemma_predict.pl -i $geno -bslmm $bslmm -n $nruns -q $queue ; cd ../../../ \" ::: {1..$perms} ::: {1..$nreps}");


# --------------------------------------------------------------------------------------------


print "Command to submit the jobs to Iceberg ($queue queue):\n\n";
print "parallel \"cd permutations/perm{1}/rep{2} ; qsub output/gemma_* ; cd ../../../ \" ::: {1..$perms} ::: {1..$nreps} \n\n";

# ==============================================================================
# ==============================================================================
# ============================== SUBROUTINES ===================================
# ==============================================================================
# ==============================================================================

# Show copyright
# ==============================================================================
sub author{
    print "\n";
    print "#########################################\n";
    print "  ".basename($0)."\n";
	print "  version $version\n";
    print "  (c) Stuart Dennis             \n";
    print "  stuart.dennis\@sheffield.ac.uk      \n";
    print "#########################################\n";
	print "\n";
}
# ==============================================================================

# Show usage
# ==============================================================================
sub usage{
    print "\n";
	print "  Usage:\n";
    print "    ".basename($0)."\n";
	print "      -i <input file (.geno file)>\n";
	print "      -o <output prefix>\n";
	print "      -n <no. of chains to run (optional, default=5)>\n";
	print "      -bslmm <bslmm type (<int> optional, 1= 'linear', 2= 'ridge regression', 3= 'probit' (default=3))>\n";
	print "      -perms <no of permutations to run (<int> optional, default = 5)>\n";
	print "      -train <percent of phenotypes to use as training set (<int> optional (default=90))>\n\n";
	print "      -q <queue (iceberg|popgenom|molecol, optional, default=iceberg)>\n";
	print "      -h <show this help>\n";
    print "\n";
    exit;
}
# ==============================================================================
