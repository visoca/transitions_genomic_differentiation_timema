#! /usr/bin/env perl

# (c) Stuart Dennis
# stuart.dennis@sheffield.ac.uk
# Last modified: 18/12/2014 13:35:17

# Description:
# This script creates initial input files for GEMMA analyses
# and generates a submission script for Iceberg compute cluster at The University of Sheffield.
# This script will require modification to run on other compute clusters but is not supported.
# Requirements:
# 1.a .pheno file containing phenotype information
# 2. a genotype file (.bbgeno, .geno or .geno.csv) - must include 2-allele columns (eg A C)

use warnings;
use strict;
use Getopt::Long;
use File::Basename;
use File::Path qw(make_path);
use File::Spec;
use Statistics::R;
use POSIX qw(strftime);

my $date=strftime("%Y%m%d%-H%M%S", localtime);

my $version='1.0.2-2014.12.18';
my $ncpu=1;

# GEMMA bslmm default arguments
my $gemma='gemma';
my $bslmm=3;
my $burnin=5000000;
my $mcmc=20000000;
my $rpace=100;
my $wpace=10000;
my $hmin=0;
my $hmax=1;
my $rmin=0;
my $rmax=1;
my $maf=0.01;
my $outdir='output';

# SGE arguments
my $hrs=168;
my $mem=4;
my $queue='iceberg';
my $email='';
my $nruns=10;
my $node=`hostname`;
chomp($node);

&author;
my ($infile);
GetOptions(
    'i|I=s'  => \$infile,
    'o|O=s'  => \$outdir,
    'bslmm=i' => \$bslmm,
    'wpace=i' => \$wpace,
    'n|N=i'  => \$nruns,
    'burn=i' => \$burnin,
    'mcmc=i' => \$mcmc,
    'rpace=i' => \$rpace,
	'hmin=i' => \$hmin,
	'hmax=i' => \$hmax,
	'rmin=i' => \$rmin,
    'rmax=i' => \$rmax,
    'maf=i' => \$maf,
    'hrs=i'  => \$hrs,
    'q|Q=s'  => \$queue,
    'm|M=i'  => \$mem,
    'e|E=s'  => \$email,
    'h|help' => \&usage
);

&usage if (!defined($infile) || !defined($outdir));

# SGE options
my $vmem=$mem+2;
$mem=$mem.'G';
$vmem=$vmem.'G';


# create seeds
my $seeds='';
my $s1=my $s2=-1;
     foreach my $i (1..$nruns){
         while ($s1 == $s2){
             $s2=int(rand(100000000));
         }
     $s1=$s2;
     $seeds.=$s2.' ';
}
$seeds=~ s/\ $//g;


#Input file
if (! -e $infile){
die ("\nCan't find input file: $infile\n\n");
}
$infile=File::Spec->rel2abs($infile);

my $pheno=<*.pheno>;
$pheno=File::Spec->rel2abs($pheno);
if (! -e $pheno){
die ("\nCan't find phenotype file: \n  Unable to continue, Quitting \:\( \n\n");
}
else{
print "Phenotype file $pheno found\n\n";
}


# Output directory
if (! -e $outdir){
eval {make_path($outdir)}
or die ("\nError creating output directory: $outdir\n\n");
}
$outdir=File::Spec->rel2abs($outdir);

# Output files
my $outbase=basename($infile);
$outbase=~ s/\.geno//g;
$outbase=~ s/\.bbgeno//g;

my $geno=basename($infile);
$geno=~ s/\.geno/\.geno/g;
$geno=File::Spec->rel2abs($geno);

my $snploc=basename($infile);
$snploc=~ s/\.geno/\.snploc/g;
$snploc=~ s/\.bbgeno/\.snploc/g;
$snploc=File::Spec->rel2abs($snploc);


my $smsjob="$outdir/gemma_".basename($infile).".$date.smsjob.sh";
my $smslog="$outdir/gemma_".basename($infile).".$date.smsjob.log";

# Calculate genotype probabilities
if (! -e $geno){
print " Genotype file not found.  You must supply a genotype file \n\n"
}
else {
print "Genotype file $geno found\n\n";
}

# Create SNP location file
if (! -e $snploc){
print "Creating SNP location file...";
my $awkward=qq(awk -F"-" '{OFS="" ; print \$0," " \$2," " \$1 }');
system("cut -d' ' -f1  $geno | $awkward > $snploc ");
print "Done\n\n";
}
else {
print "SNP location file $snploc found \n\n";
}
#if it doesn't already exist, compute relatedness matrix type: 1 = centered, 2 = standardized
my $matrixtype=1;

#create relatedness matrix file

my $relmat="output/$outbase.cXX.txt";

if (! -e $relmat){
print "Generating relatedness matrix...";
system("$gemma -g $geno -p $pheno -gk $matrixtype -o $outbase");
}
else {
print "Relatedness matrix $relmat found \n\n";
}
if(! -e $relmat){
	die ("\nSomething went wrong creating relatedness matrix - check the format of your genotype file and try again\n\n");
	}
# define prediction method
my $pred = ();

if ($bslmm == 3){
		$pred=2;
      print "Setting predictions for probit model\n\n";
}
else{
       $pred=1;
      print "\n\nSetting predictions for regression\n\n";
}

# --------------------------------------------------------------------------------------------

# Create GEMMA submission script
open (FILE, ">$smsjob")
	or die ("\nCan't open $smsjob\n\n");

print FILE <<EOF;
#!/bin/bash
#\$ -l h_rt=$hrs:00:00
#\$ -l rmem=$mem
#\$ -l mem=$vmem
#\$ -j y
#\$ -o $smslog
#\$ -t 1-$nruns
EOF
if ($email ne ''){
	print FILE '#$ -m bea'."\n";
	print FILE '#$ -M '.$email."\n";
}
if ($queue eq 'popgenom' || $queue eq 'molecol'){
	print FILE '#$ -P '.$queue."\n";
	print FILE '#$ -q '.$queue.".q\n";
}


print FILE <<EOF;

GEMMA=$gemma
GENO=$geno
PHENO=$pheno
SNPLOC=$snploc
RELMAT=$relmat
BSLMM=$bslmm
BURNIN=$burnin
RPACE=$rpace
WPACE=$wpace
MCMC=$mcmc
RMIN=$rmin
RMAX=$rmax
HMIN=$hmin
HMAX=$hmax
MAF=$maf
PRED=$pred
GENONAME=\$(basename \$GENO)
RUNINDEX=\$((\$((SGE_TASK_ID-1)) % $nruns + 1))
OUTDIR=$outdir
SEED=($seeds)
echo GENO: \$GENO - RUNINDEX: \$RUNINDEX

OUTFILE="\${GENONAME%.*}"_"\$RUNINDEX"
IDLOG="\$OUTDIR/\${GENONAME%.*}"_"\$RUNINDEX.log"

echo "Array job ID: \$SGE_TASK_ID" > \$IDLOG
echo

# echo \$OUTFILE - \$OUTPOST - \$IDLOG

hostname >> \$IDLOG
uname -a >> \$IDLOG
date >> \$IDLOG
echo "---------------------------------------------------" >> \$IDLOG
echo >> \$IDLOG
echo "Running gemma for \$GENO..." >> \$IDLOG
echo >> \$IDLOG
echo "CMD: " >> \$IDLOG
echo "    \$GEMMA \\\\" >> \$IDLOG
echo "      -g \$GENO \\\\" >> \$IDLOG
echo "		-p \$PHENO \\\\">> \$IDLOG
echo "		-a \$SNPLOC \\\\">> \$IDLOG
echo "		-k \$RELMAT \\\\">> \$IDLOG
echo "		-o \$OUTFILE \\\\">> \$IDLOG
echo "		-bslmm \$BSLMM \\\\">> \$IDLOG
echo "		-w \$BURNIN \\\\">> \$IDLOG
echo "		-s \$MCMC \\\\">> \$IDLOG
echo "		-rpace \$RPACE \\\\">> \$IDLOG
echo "		-wpace \$WPACE \\\\">> \$IDLOG
echo "		-hmin \$HMIN \\\\">> \$IDLOG
echo "		-hmax \$HMAX \\\\">> \$IDLOG
echo "		-maf \$MAF \\\\">> \$IDLOG
echo "		-rmin \$RMIN \\\\">> \$IDLOG
echo "		-rmax \$RMAX \\\\">> \$IDLOG
echo "		-seed \${SEED[\$((SGE_TASK_ID-1))]} \\\\" >> \$IDLOG
echo >> \$IDLOG
echo "---------------------------------------------------" >> \$IDLOG
echo >> \$IDLOG

\$GEMMA \\
-g \$GENO \\
-p \$PHENO \\
-a \$SNPLOC \\
-k \$RELMAT \\
-bslmm \$BSLMM \\
-w \$BURNIN \\
-s \$MCMC \\
-rpace \$RPACE \\
-wpace \$WPACE \\
-maf \$MAF \\
-hmin \$HMIN \\
-hmax \$HMAX \\
-rmin \$RMIN \\
-rmax \$RMAX \\
-seed \${SEED[\$((SGE_TASK_ID-1))]} \\
-o \$OUTFILE  >> \$IDLOG 2>&1
echo >> \$IDLOG
echo "---------------------------------------------------" >> \$IDLOG
echo >> \$IDLOG
date >> \$IDLOG
echo >> \$IDLOG
echo "------------Predictions--------------------------" >> \$IDLOG
#new section for generating predicted values

\$GEMMA \\
-g \$GENO \\
-p \$PHENO \\
-a \$SNPLOC \\
-k \$RELMAT \\
-epm output/\$OUTFILE.param.txt \\
-emu output/\$OUTFILE.log.txt \\
-ebv output/\$OUTFILE.bv.txt \\
-predict \$PRED \\
-o \$OUTFILE  >> \$IDLOG 2>&1
echo >> \$IDLOG
echo "---------------------------------------------------" >> \$IDLOG
echo >> \$IDLOG
date >> \$IDLOG
echo >> \$IDLOG

EOF

close (FILE);

system("chmod +x $smsjob");

print "Command to submit the job to Iceberg ($queue queue):\n\n";
print "qsub $smsjob\n\n";

# ==============================================================================
# ==============================================================================
# ============================== SUBROUTINES ===================================
# ==============================================================================
# ==============================================================================

# Show copyright
# ==============================================================================
sub author{
    print "\n";
    print "#########################################\n";
    print "  ".basename($0)."\n";
	print "  version $version\n";
    print "  (c) Stuart Dennis             \n";
    print "  stuart.dennis\@sheffield.ac.uk      \n";
    print "#########################################\n";
	print "\n";
}
# ==============================================================================

# Show usage
# ==============================================================================
sub usage{
    print "\n";
	print "  Usage:\n";
    print "    ".basename($0)."\n";
	print "      -i <input file (.geno file)>\n";
	print "      -o <output prefix>\n";
	print "      -n <no. of chains to run (optional, default=$nruns)>\n";
	print "      -burn <MCMC burnin (optional, default=$burnin)>\n";
	print "      -mcmc <MCMC length (optional, default=$mcmc)>\n";
	print "      -rpace <MCMC length (optional, default=$rpace)>\n";
	print "      -wpace <MCMC length (optional, default=$wpace)>\n";
    print "      -hmin <h prior MIN (optional, default=$hmin)>\n";
	print "      -hmax <h prior MAX (optional, default=$hmax)>\n";
    print "      -rmin <rho prior MIN (optional, default=$rmin)>\n";
    print "      -rmax <rho prior MAX (optional, default=$rmax)>\n";
    print "      -maf <minor allele frequency (optional, default=$maf)>\n";
	print "      -bslmm <bslmm type (<int> optional, 1= 'linear', 2= 'ridge regression', 3= 'probit' (default=$bslmm))>\n";
	print "      -hrs <max. time allocated in hours (optional, default=$hrs)>\n";
	print "      -q <queue (iceberg|popgenom|molecol, optional, default=$queue)>\n";
	print "      -m <memory (optional, default=$mem)>\n";
	print "      -e <email (optional, default=none)>\n";
	print "      -h <show this help>\n";
    print "\n";
    exit;
}
# ==============================================================================
