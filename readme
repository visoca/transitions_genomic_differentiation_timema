This repository contains a compilation of programs and scripts used for processing
and analysing data for: Riesch R, Muschick M, Lindtke D, Villoutreix R, Comeault AA,
Farkas TE, Lucek K, Hellen E, Soria-Carrasco V, Dennis SR, de Carvalho CF, 
Safran RJ, Sandoval CP, Feder J, Gries R, Crespi BJ, Gries G and Nosil P (2017) 
Transitions between phases of genomic differentiation during stick-insect speciation. 
Nature Ecology & Evolution, 1, 0082 http://dx.doi.org/10.1038/s41559-017-0082. 
More detailed information can be found in the folders for each particular analysis in
the Dryad repository http://dx.doi.org/10.5061/dryad.nq67q and in the Online 
Supplementary Materials.

  admixture
    This directory contains entropy (c++ program) and Perl scripts to facilitate
	running admixture analyses.
	  
	  entropy_1.2c.tar.gz
	  	This compressed directory contains an slightly updated version of the
	  	C++ program implementing the admixture proportion and admixture class 
	  	models described in Gompert et al. 2014 doi: 10.1111/mec.12811.
	  
	  entropy.pl 
	    Perl script to prepare input data for analysis with entropy. It depends
		on scripts bcf2gl.pl and bcf2bbgeno.pl (see below) 
	  
	  entropy_output.pl 
	    Perl script to summarize entropy output
	  
	  entropy_diagnostics.pl -> produce extra plots to checd entropy runs

	  bcf2gl.pl  
		Perl script to convert a bcf file to the genotype likelihood (gl) format
		required by entropy scripts
			  
	  bcf2bbgeno.pl
		Perl script to convert a bcf file to the BIMBAM format required by entropy 
		scripts

  fst
	This directory contains scripts for estimating Hudson's FST and also 
	converting bcf to the required formats. It also includes a C++ program called
	estpEM that allows estimating allele frequencies from genotype likelihoods.
	 
	 estpEM_2014-10-08.tar.gz:
		C++ program for estimating allele frequencies from genotype likelihoods using 
		a soft EM algorithm (see equation 5 in doi:10.1093/bioinformatics/btr509 for 
		mathematical details)
     
	 fst.pl:
		Perl script to calculate Hudson's Fst for population pairs from a BCF file.
		Syntax help shown with: fst.pl -h
		Example of syntax used for estimating Fsts for population pairs:
			fst.pl \
			-i variants.flt.100kmreads_10kdepth_20QS.90cov.phylob.bcf \
			-p aux/bart_BMCG3_IC_vs_bart_BMCG3_WF.dsv \
			-o loci/bart_BMCG3_IC_vs_bart_BMCG3_WF.fst.dsv \
			-ms 0.5  \
			-mf 0.05 \
			-pr 0 \
			-am 0 \
			-fm 0 \
			-sep 0

  gwas
    This directory contains a set of scripts to run GWA analyses with gemma. 
    Further information	can be found in the readme file inside the directory
  
  phylogenetics
    This directory contains scripts used for phylogenetic analyses
	
	phy2nex.pl:
	  Perl script to convert multiple alignments from phylip to nexus
	
	bcf2fa.pl
	  Perl script to convert a bcf file to a a multiple alignment in fasta format.
	  It allows generating consensus sequences, and splitting output by regions
	  by providing the appropriate input files. It also allows applying several
	  filters 

  wgs_fst_dxy_pi_hmm
    This directory contains scripts used to calculate Fst, Dxy, Da and pi in 
    sliding windows along genomes, and to fit HMMs for these statistics. More
    information can be found in the readme file inside the directory.

