#!/usr/bin/env perl

# (c) Victor Soria-Carrasco
# Last modified: 31/05/2016 19:38:08

# Description:
# This script estimate fsts for two populations defined
# by a set of individuals using a bcf file as input (which can
# contain other populations as well)

# Mean genotype probabilities can be estimated using a uniform or
# a Hardy-Weinberg equilibrium prior
# Be aware that SNPs with >2 alleles will be excluded

# population pair file must have the following structure:
#
# ind1 pop1
# ind2 pop1
# ind3 pop2
# ind4 pop2

# windows file must have the following structure:
#
# scaffold lower_bound upper_bound

# Changelog:
#  1.22 - 31/05/2016 - fixed small bug - uninitialized warning when (wrongly)
#				       trying to sort windows when no windows specified

use strict;
use warnings;
use Getopt::Long;
use File::Basename;
use File::Path qw(make_path remove_tree);

my $version='1.22-2016.05.31';

# Path to external programs
my $bcftools='bcftools';
my $estpEM='estpEM'; # program for estimating AFs by ML

&author;

# Read arguments and set up output directories/files
# =============================================================================
my $prior=0; # 0-> uniform, 1-> H-W
my $mincov="0.5";
my $mmaf=0.05; # Minimum MAF allowed
my $separate=0;
my $afmethod=0; #0-> AFs estimated from empirical Bayesian genotypes, 1-> AFS estimated using EM algorithm
my $fstmethod=0; #0-> Hudson, 1->HudsonLoci, 2->Weir&Cockerham
my %fstmethod_names=(
	0=>"HudsonFst",
	1=>"HudsonFstloci",
	2=>"Weir&CockerhamFst");
my $keeptmp=0;
my $ignorelg=0; # ignore lg and order for windows

&usage if (scalar(@ARGV)==0);

my ($infile, $outfile, $poppair, $windowsfile, $outfile2);
GetOptions(
    'i|I=s'     => \$infile,
    'p|P=s'     => \$poppair,
    'w|W=s'     => \$windowsfile,
    'o|O=s'     => \$outfile,
    'ow|OW=s'   => \$outfile2,
    'pr|PR=i'   => \$prior,
    'ms|MS=s'   => \$mincov,
    'mf|MF=s'   => \$mmaf,
    'am|AM=i'   => \$afmethod,
    'fm|FM=i'   => \$fstmethod,
	'sep|SEP=i' => \$separate,
	'k|K=i'     => \$keeptmp,
	'ilg=i'		=> \$ignorelg,
    'h|help'    => \&usage
)
or &usage("\nERROR: Argument invalid (see above)\n\n");


# Check arguments
# ----------------------------------------------------------
my @errors=();
if (!defined($infile)){
	push (@errors, "bcf/vcf input file not specified");
}
else{
	push (@errors, "bcf/vcf input file does not exist or cannot be reached (you typed '-i $infile')")
		if (! -e $infile);
}
if (!defined($poppair)){
	push (@errors, "population pair input file not specified");
}
else{
	push (@errors, "population pair inputfile does not exist or cannot be reached (you typed '-p $poppair')")
		if (! -e $poppair);
}
if (!defined($outfile)){
	push (@errors, "output file not specified");
}
if (defined($windowsfile) && !defined($outfile2)){
	push (@errors, "output file for genomic windows not specified");
}
if ($prior !~ m/^[0|1]$/){
	push (@errors, "prior must be 0 (uniform) or 1 (Hardy-Weinberg) (you typed '-pr $prior')");
}
if ($mincov <= 0 || $mincov >= 1){
	push (@errors, "minimum sampling coverage must be a number >0 and <1 (you typed '-ms $mincov')");
}
if ($mmaf < 0 || $mmaf >=0.5){
	push (@errors, "minimum minor allele frequency be a number >=0 and <0.5 (you typed '-mf $mmaf')");
}
if ($afmethod !~ m/^[0|1]$/){
	push (@errors, "use EM algorithm to estimate AF must be 0 (no) or 1 (yes) (you typed '-am $afmethod')");
}
if ($fstmethod !~ m/^[0|1|2]$/){
	push (@errors, "method for Fst estimation must be 0 (Hudson), 1 (Hudson loci), or 2 (Weir-Cockerham) (you typed '-fm $fstmethod')");
}
if ($separate !~ m/^[0|1]$/){
	push (@errors, "estimate the AF separately for each population must be 0 (no) or 1 (yes) (you typed '-sep $separate')");
}
if ($keeptmp !~ m/^[0|1]$/){
	push (@errors, "keep the temporary files must be 0 (no) or 1 (yes) (you typed '-k $keeptmp')");
}

if (scalar(@errors)>0){
	my $out="\n\nThere were ERRORS:\n";
	foreach my $e (@errors){
		$out.="\t$e\n";
	}
	$out.="\n\n";
	&usage($out);
}
# ----------------------------------------------------------

# Adjust options
# ----------------------------------------------------------
if ($afmethod==1 && $prior==0 && $separate==0){
	print "\nWarning: using uniform priors and EM algorithm implies AFs are estimated separately\n\n";
	$separate=1;
}
# ----------------------------------------------------------

# Absolute paths to files
# ----------------------------------------------------------
$infile=File::Spec->rel2abs($infile);
$poppair=File::Spec->rel2abs($poppair);
$windowsfile=File::Spec->rel2abs($windowsfile) if (defined($windowsfile));
$outfile=File::Spec->rel2abs($outfile);
$outfile2=File::Spec->rel2abs($outfile2) if (defined($windowsfile));
# ----------------------------------------------------------

# temporary files
# ----------------------------------------------------------
my $randpart=$ENV{USER}."-".time()."-".int(rand(1000000));
my $tmp=dirname($outfile)."/tmp_".$randpart;
my $sampfile="$tmp/samp_".$randpart;
my $scafile="$tmp/sca_".$randpart;
my $glfile="$tmp/gl_".$randpart; # genotype likelihood
my $afemfile="$tmp/afem_".$randpart;
# ----------------------------------------------------------


# Read population pair file and create file with samples
# ----------------------------------------------------------
my %sampop=();
my @samples=();
my @pops=();
open (FILE, "$poppair")
	or die ("\nCan't open populations file $poppair\n\n");
	while (<FILE>){
		next if (/^(\#|\s*\n)/);
		my @aux=split(/\s|\,/,$_);
		$aux[0]=~ s/(\.sorted)?\.[b|s]am//g; # remove any possible suffixes
		push (@samples, $aux[0]);
		$sampop{$aux[0]}=$aux[1];
		push (@pops, $aux[1]) if (!grep(/^$aux[1]$/,@pops));
	}
close (FILE);
#----------------------------------------------------------

# Read windows file
# ----------------------------------------------------------
# Format must be scaffold\tlb\tub (with header)
my %windows;
my $i=0;
my $sca='';
if (defined($windowsfile)){
	open (FILE, "$windowsfile")
		or die ("\nCan't open genomic windows file $windowsfile\n\n");
		while (<FILE>){
			next if ($.==1 && !/[0-9]/); # header
			next if (/^(\#|\s*\n)/); # comments and empty lines
			my @aux=split(/\s|\,/,$_);
			# print "line $_ -- $aux[0]-$i-lb $aux[1]\n";
			$i=0 if ($aux[0] ne $sca);
			$windows{$aux[0]}[$i]{'lb'}=$aux[1];
			$windows{$aux[0]}[$i]{'ub'}=$aux[2];
			$sca=$aux[0];
			$i++;
		}
	close (FILE);
}
# ----------------------------------------------------------

# Calculate number of samples per population
my %nsamples=();
foreach my $s (keys %sampop){
	$nsamples{$sampop{$s}}++;;
}

# Exit if # pops > 2 in poppair file
die ("\nERROR: More than 2 populations in the population file $poppair\n\n")
	if (scalar(@pops)>2);

# Exit if # samples is <2 for any population
foreach my $p (@pops){
	die ("\nERROR: Fewer than 2 individuals in population $p\n\n")
		if ($nsamples{$p} < 2);
}
# ----------------------------------------------------------

# Determine if format is vcf or bcf for adding argument for bcftools
# ----------------------------------------------------------
my $arg='';
if ($infile =~ m/\.vcf$/){
	$arg='-S ';
}
elsif ($infile !~ m/\.bcf/){
	die ("\n$infile is not a bcf/vcf file\n\n");
}
# ----------------------------------------------------------

# Create temporary directory and file with samples
# ----------------------------------------------------------

make_path("$tmp");
open (TMP, ">$sampfile")
	or die ("\nCan't write to file $sampfile\n\n");
	foreach my $s (@samples){
		print TMP "$s\n";
	}
close (TMP);
# ----------------------------------------------------------

# Create scaffold file (sequence dictionary necessary for vcf)
# ----------------------------------------------------------
open (FILE, "$bcftools view $arg$infile | head -n 15000 | ")
	or die ("\nCan't open $infile\n\n");
open (SCA, ">$scafile")
	or die ("\nCan't write to $scafile\n\n");
	while (<FILE>){
		if (/^\#\#.*(scaffold_[0-9]+)/ ||
			/^\#\#.*(lg([0-9]+|NA)_ord([0-9]+|NA)_scaf[0-9]+)/ ||
			/^\#\#.*(pseudoscaff_[0-9]+)/){
			print SCA "$1\n";
		}
	}
close (SCA);
close (FILE);

if (-z $scafile){
	my %scafs=();
	open (FILE, "$bcftools view $arg$infile | ")
		or die ("\nCan't open $infile\n\n");
		while (<FILE>){
			next if (/^\#/);
			my @aux=split(/\s+/,$_);
			$scafs{$aux[0]}=1;
		}
	close (FILE);
	open (SCA, ">$scafile")
		or die ("\nCan't write to $scafile\n\n");
		foreach my $s (sort keys %scafs){
			print SCA "$s\n";
		}
	close (SCA);
}
# ----------------------------------------------------------

# Convert from bcf to gl
# (save matrix into memory if it is not too big)
# ----------------------------------------------------------
my $in=("$bcftools view $arg$infile 2> /dev/null | \\
		perl -pi -e 's/(\.sorted)?\.[b|s]am//g if (/^\#CHROM/)' | \\
		$bcftools view -Sv -d $mincov -s $sampfile -D $scafile - 2> /dev/null |");

my ($x, $y, $z)=&bcf2gl($in, $glfile);
my @genolhl=@$x;
my @bcf_af=@$y;
my @ids=@$z;

# Read glfile and split by population
# This is all assuming samples will be in the same order as in the samples file
if ($separate==1){
	my %ids_files;
	foreach my $i (0..$#pops){
		open (my $fh, ">$glfile.$i")
			or die ("\nCan't write to file $glfile.$i\n\n");
		$ids_files{$pops[$i]}=$fh;
		print {$ids_files{$pops[$i]}} "$nsamples{$pops[$i]} ".scalar(@genolhl)."\n";
	}

	foreach (@genolhl){
		chomp;
		my @aux=split(/\s+/,$_);
		# print SNP id
		my $snpid=shift(@aux);

		foreach my $p (@pops){
			print {$ids_files{$p}} "$snpid";
		}
		# print the genotype likelihoods to the corresponding files
		# IMPORTANT: GL go in 3 by 3
		my $i=0;
		while ($i < $#aux){
			print {$ids_files{$sampop{$samples[$i/3]}}} " $aux[$i] $aux[$i+1] $aux[$i+2]";
			$i+=3;
		}
		# print linebreak
		foreach my $p (@pops){
			print {$ids_files{$p}} "\n";
		}
	}

	foreach my $p (keys %ids_files){
		close ($ids_files{$p});
	}
}
# ----------------------------------------------------------

# Calculate AFs with EM algorithm
# ----------------------------------------------------------
my %afl=(); # AF for each locus
if ($afmethod==1){ # use EM algorithm to estimate allele frequencies
	print "Estimating allele frequencies by maximum likelihood...\n\n";

	# Note: global AF estimation is always needed to filter out by MAF
	# system("$estpEM -i $glfile -o $afemfile -m 50 -e 0.0001 >& $afemfile.log");
	system("$estpEM -i $glfile -o $afemfile -m 20 -e 0.001 >& $afemfile.log");
	open (FILE, "$afemfile")
		or die ("\nCan't open file $afemfile\n\n");
		while (<FILE>){
			chomp;
			my @aux=split(/\s+/,$_);
			$afl{$aux[0]}=$aux[2];
		}
	close (FILE);

	if ($separate==1){
		system("$estpEM -i $glfile.0 -o $afemfile.0 -m 20 -e 0.001 >& $afemfile.0.log");
		system("$estpEM -i $glfile.1 -o $afemfile.1 -m 20 -e 0.001 >& $afemfile.1.log");

		open (FILE, "$afemfile.0")
			or die ("\nCan't open file $afemfile.0\n\n");
			while (<FILE>){
				chomp;
				my @aux=split(/\s+/,$_);
				$afl{$pops[0]}{$aux[0]}=$aux[2];
			}
		close (FILE);

		open (FILE, "$afemfile.1")
			or die ("\nCan't open file $afemfile.1\n\n");
			while (<FILE>){
				chomp;
				my @aux=split(/\s+/,$_);
				$afl{$pops[1]}{$aux[0]}=$aux[2];
			}
		close (FILE);

	}

	print "Done\n\n";
}
# ----------------------------------------------------------

# Calculate Fst for each locus
# =============================================================================
my $nsnps=0;
my @fstloci=();
my @nums=(); # numerators for calculating mean Fst
my %numsloci=(); # numerators for calculating mean Fst per window
my @denoms=(); # denominator for calculating mean Fsts
my %denomsloci=(); # denominators for calculating mean Fst per window

foreach my $i (0..$#genolhl){ # For each locus (i.e. SNP)
	print "SNPs processed (used for Fst estimation): $nsnps\r" if ($nsnps%100==0);
	$|++; # flush buffer

	my @aux=split(/\s+|\:/,$genolhl[$i]);
	# my $snpid=shift(@aux);
	my $sca=shift(@aux);
	my $pos=shift(@aux);
	my $snpid=$sca.':'.$pos;

	# Calculate AFs
	# ------------------------------------------------------------------
	my $af='';
	my %afs=(); # allele frequencies of each population
	my %genotypes=(); # required when using uniform prior
	if ($afmethod==0 && $prior==0){ # use empirical Bayesian genotypes with uniform prior
		# Estimate allele frequency using genotype probabilities
		# with uniform priors (doesn't require knowing AFs beforehand)
		# --------------------------------------------------------
		# Note: @aux only contains genotype likelihoods, first position storing snp id was removed before
		my $j=0;
		while ($j < $#aux){
			push (@{$genotypes{$sampop{$ids[$j/3]}}},&pl2cgp_uni($aux[$j],$aux[$j+1],$aux[$j+2]));
			$j+=3;
		}

		# Calculate allele frequencies for each population
		# ------------------------------------------------
		foreach my $p (keys %genotypes){
			$afs{$p}=sum(@{$genotypes{$p}})/(2*scalar(@{$genotypes{$p}}))
		}
		# ------------------------------------------------

		# AF of 2 pops together (required only for MAF filter)
		$af=sum((@{$genotypes{$pops[0]}},@{$genotypes{$pops[1]}}))/(2*(scalar(@{$genotypes{$pops[0]}})+scalar(@{$genotypes{$pops[1]}})));
	}
	elsif ($afmethod==0 && $prior==1){ # Hardy-Weinberg priors using AFs coming from bcf file
		$af=$bcf_af[$i]; # AF from bcftools
		if ($separate==0){
			# Estimate genotypes, which will be used for estimating
			# AFs for each population
			# Note: @aux only contains genotype likelihoods, first position storing snp id was removed before
			my $j=0;
			while ($j < $#aux){
				my @gt=($aux[$j],$aux[$j+1],$aux[$j+2]);
				push (@{$genotypes{$sampop{$ids[$j/3]}}},&pl2cgp_hw(\@gt,\$af));
				$j+=3;
			}
			# Calculate allele frequencies for each population
			# ------------------------------------------------
			foreach my $p (keys %genotypes){
				$afs{$p}=sum(@{$genotypes{$p}})/(2*scalar(@{$genotypes{$p}}))
			}
			# ------------------------------------------------
		}
		else{
			# ToDo - extract population specific AF from bcf file
			# $afs{$pops[0]}=$afl{$pops[0]}{$snpid};
			# $afs{$pops[1]}=$afl{$pops[1]}{$snpid};
		}
	}
	elsif ($afmethod==1 && $prior==0){ # uniform prior implies AFs must be separated
		$af=$afl{$snpid};
		$afs{$pops[0]}=$afl{$pops[0]}{$snpid};
		$afs{$pops[1]}=$afl{$pops[1]}{$snpid};
	}
	elsif ($afmethod==1 && $prior==1){ # Hardy-Weinberg prior using AFs estimated with EM algorithm
		$af=$afl{$snpid};
		# print "AF EM: $snpid - $af\n";
		if ($separate==0){
			# Estimate genotypes, which will be used for estimating
			# AFs for each population
			# Note: @aux only contains genotype likelihoods, first position storing snp id was removed before
			my $j=0;
			while ($j < $#aux){
				my @gt=($aux[$j],$aux[$j+1],$aux[$j+2]);
				push (@{$genotypes{$sampop{$ids[$j/3]}}},&pl2cgp_hw(\@gt,\$af));
				$j+=3;
			}
			# Calculate allele frequencies for each population
			# ------------------------------------------------
			foreach my $p (keys %genotypes){
				$afs{$p}=sum(@{$genotypes{$p}})/(2*scalar(@{$genotypes{$p}}))
			}
			# ------------------------------------------------
		}
		else{
			# Estimate genotypes, which will be used for estimating
			# AFs for each population
			# Note: @aux only contains genotype likelihoods, first position storing snp id was removed before
			my $j=0;
			while ($j < $#aux){
				my @gt=($aux[$j],$aux[$j+1],$aux[$j+2]);
				push (@{$genotypes{$sampop{$ids[$j/3]}}},&pl2cgp_hw(\@gt,\$afl{$sampop{$ids[$j/3]}}{$snpid}));
				$j+=3;
			}
			# Calculate allele frequencies for each population
			# ------------------------------------------------
			foreach my $p (keys %genotypes){
				$afs{$p}=sum(@{$genotypes{$p}})/(2*scalar(@{$genotypes{$p}}))
			}
			# ------------------------------------------------
		}
	}
	# ------------------------------------------------------------------

	# minor allele frequency (for filtering)
	# --------------------------------------------------------
	my $maf=$af;
	$maf=1-$af if ($af>0.5);
	# -------------------------------------------------------

	if ($af < 1 &&     # exclude 'invariant' SNPs: all individuals have the same allele, but it is different from reference
		$maf > $mmaf){ # exclude SNPs with MAF < $mmaf

		# Estimate dAF and Fst for each locus
		# ------------------------------------------------
		my $dAF=sprintf("%.6f", $afs{$pops[0]}-$afs{$pops[1]});
		my ($fst,$num,$den);
		if ($fstmethod == 0){
			($fst,$num,$den)=HudsonFst($afs{$pops[0]},$afs{$pops[1]});
		}
		elsif ($fstmethod == 1){ # Hudson loci
			($fst,$num,$den)=HudsonFstloci($afs{$pops[0]},$afs{$pops[1]},$nsamples{$pops[0]},$nsamples{$pops[1]});
		}
		elsif ($fstmethod == 2){ # Weir & Cockerham
			($fst,$num,$den)=WeirCockerhamFst($afs{$pops[0]},$afs{$pops[1]},$nsamples{$pops[0]},$nsamples{$pops[1]});
		}

		$fst=sprintf("%.6f", $fst);
		push (@fstloci, "$sca\t$pos\t$dAF\t$fst");
		# ------------------------------------------------

		# Store numerators and denominators for estimating
		# genome-wide Fst, and window Fst at the end
		# ------------------------------------------------
		push (@nums, $num);
		$sca=~ s/.*scaf//g if ($ignorelg==1);
		$numsloci{$sca}{$pos}=$num;
		push (@denoms, $den);
		$denomsloci{$sca}{$pos}=$den;
		# ------------------------------------------------

		$nsnps++;
	}
}
print "SNPs processed (used for Fst estimation): $nsnps\r";
print "\n\n";

# Estimate genome-wide Fst
# ----------------------------------------------------------
my $meanfst=0;
if ($fstmethod==0 || $fstmethod==2){
	$meanfst=sprintf("%.6f", (1-mean(@nums)/mean(@denoms)));
}
elsif ($fstmethod==1){
	$meanfst=sprintf("%.6f", (mean(@nums)/mean(@denoms)));
}

print "pop1\tpop2\tmean".$fstmethod_names{$fstmethod}."\n";
print "$pops[0]\t$pops[1]\t$meanfst\n\n";
# ----------------------------------------------------------

# Sort loci by linkage group -> scaffold -> position
# ----------------------------------------------------------
# Remove NAs with a number for numerical sorting
if ($fstloci[0]=~ /^lg/){
	foreach my $fl (@fstloci){
		$fl=~ s/NA/999999999/g;
	}
	@fstloci=sort {
			my ($alg) = $a =~ /lg([0-9]+)/;
			my ($blg) = $b =~ /lg([0-9]+)/;
			my ($aord) = $a =~ /ord([0-9]+)/;
			my ($bord) = $b =~ /ord([0-9]+)/;
			my ($ascaf) = $a =~ /scaf*([0-9]+)/;
			my ($bscaf) = $b =~ /scaf*([0-9]+)/;
			$alg <=> $blg || $aord <=> $bord || $ascaf <=> $bscaf;
			} @fstloci;
	# Put back NAs
	foreach my $fl (@fstloci){
		$fl=~ s/999999999/NA/g;
	}
}
elsif ($fstloci[0]=~ /^scaffold/){
	@fstloci=sort {
			my ($ascaf) = $a =~ /scaffold_([0-9]+)/;
			my ($bscaf) = $b =~ /scaffold_([0-9]+)/;
			$ascaf <=> $bscaf;
			} @fstloci;
}
elsif ($fstloci[0]=~ /^pseudoscaff/){
	my @sortfstloci=sort {
		my ($ascaf) = $a =~/pseudoscaff_([0-9]+)/;
		my ($bscaf) = $b =~/pseudoscaff_([0-9]+)/;
		$ascaf <=> $bscaf;
	} @fstloci;
}
# ----------------------------------------------------------

# Estimate windows Fst
# ----------------------------------------------------------
my @fstwindows=();
if (defined($windowsfile)){
	foreach my $sca (keys %windows){
		foreach my $i (0..$#{$windows{$sca}}){
			my $lb=$windows{$sca}[$i]{'lb'};
			my $ub=$windows{$sca}[$i]{'ub'};
			my @nums=();
			my @denoms=();
			if (defined ($numsloci{$sca})){
				# print "\tfound\n";
				foreach my $pos (sort {$a <=> $b} keys $numsloci{$sca}){
					# print "\t\tPOS $pos\n";
					last if ($pos > $ub);
					if ($pos > $lb && $pos < $ub){
						push (@nums, $numsloci{$sca}{$pos});
						push (@denoms, $denomsloci{$sca}{$pos});
					}
				}
			}
			my $meanfst='NA';
			if (scalar(@nums)>0){
				if ($fstmethod==0 || $fstmethod==2){
					$meanfst=sprintf("%.6f", (1-mean(@nums)/mean(@denoms)));
				}
				elsif ($fstmethod==1){
					$meanfst=sprintf("%.6f", (mean(@nums)/mean(@denoms)));
				}
			}
			my $nsnps=scalar(@nums);

			push (@fstwindows,"$sca\t$lb\t$ub\t$nsnps\t$meanfst");

		}
	}
}
# ----------------------------------------------------------

# Sort windows by linkage group -> scaffold -> position
# ----------------------------------------------------------
# Remove NAs with a number for numerical sorting
if (defined($windowsfile)){
	if ($fstwindows[0]=~ /^lg/){
		foreach my $fl (@fstwindows){
			$fl=~ s/NA/999999999/g;
		}
		@fstwindows=sort {
				my ($alg) = $a =~ /lg([0-9]+)/;
				my ($blg) = $b =~ /lg([0-9]+)/;
				my ($aord) = $a =~ /ord([0-9]+)/;
				my ($bord) = $b =~ /ord([0-9]+)/;
				my ($ascaf) = $a =~ /scaf*([0-9]+)/;
				my ($bscaf) = $b =~ /scaf*([0-9]+)/;
				$alg <=> $blg || $aord <=> $bord || $ascaf <=> $bscaf;
				} @fstwindows;
		# Put back NAs
		foreach my $fl (@fstwindows){
			$fl=~ s/999999999/NA/g;
		}
	}
	elsif ($fstwindows[0]=~ /^scaffold/){
		@fstwindows=sort {
				my ($ascaf) = $a =~ /scaffold_([0-9]+)/;
				my ($bscaf) = $b =~ /scaffold_([0-9]+)/;
				$ascaf <=> $bscaf;
				} @fstwindows;
	}
	elsif ($fstwindows[0]=~ /^pseudoscaff/){
		my @sortfstwindows=sort {
			my ($ascaf) = $a =~/pseudoscaff_([0-9]+)/;
			my ($bscaf) = $b =~/pseudoscaff_([0-9]+)/;
			$ascaf <=> $bscaf;
		} @fstwindows;
	}
}
# ----------------------------------------------------------

# =============================================================================

# Write output to files
# =============================================================================
open (FILE, ">$outfile")
	or die ("\nCan't write to file $outfile\n\n");
	print FILE "scaffold\tposition\tdAF\t".$fstmethod_names{$fstmethod}."\n";
	foreach my $l (@fstloci){
		print FILE "$l\n";
	}
close (FILE);

if (defined($windowsfile)){
	open (FILE, ">$outfile2")
		or die ("\nCan't write to file $outfile\n\n");
		print FILE "scaffold\tlb\tub\tnloci\t".$fstmethod_names{$fstmethod}."\n";
		foreach my $w (@fstwindows){
			print FILE "$w\n";
		}
	close (FILE);
}
# =============================================================================

# Remove temporary files
remove_tree($tmp) if ($keeptmp==0);

# ==============================================================================
# ==============================================================================
# ============================== SUBROUTINES ===================================
# ==============================================================================
# ==============================================================================


# Convert Phred-scaled genotype likelihoods (pl) to
# composite genotype probabilities (cgp)
# ==============================================================================

# using uniform priors
sub pl2cgp_uni{
	my @pls=@_;

	my $sum=0;
	my @gps=();
	foreach my $pl (@pls){
		my $gl=10**(-$pl/10);
		push (@gps, $gl);
		$sum+=$gl;
	}
	foreach my $pr (@gps){
		$pr=$pr/$sum;
	}
	my $cgp=$gps[2]*2+$gps[1]; # alternate allele dosage

	return($cgp);
}
# -----------------------------------------------------------------------------

# using Hardy-Weinberg priors
# -----------------------------------------------------------------------------
sub pl2cgp_hw{
	my ($a,$b)=@_;
	my @pls=@$a;
	my $af=$$b;

	# if ref allele is not the major allele
	# likelihoods must be swapped
	# minor allele frequency is then 1-af
	# if ($af>0.5){
	# 	my @aux=@pls;
	# 	$pls[0]=$aux[2];
	# 	$pls[2]=$aux[0];
	# 	$af=1-$af;
	# }

	my @gps=();
	# 1st term => Phred-descalation
	# 2nd term => Hardy-Weinberg prior
	$gps[0]=(10**(-$pls[0]/10)) * ((1-$af)**2);
	$gps[1]=(10**(-$pls[1]/10)) * (2*$af*(1-$af));
	$gps[2]=(10**(-$pls[2]/10)) * ($af**2);

	my $sum=0;
	foreach my $pr (@gps){
		$sum+=$pr
	}
	foreach my $pr (@gps){
		$pr=$pr/$sum;
	}
	my $cgp=$gps[2]*2+$gps[1]; # alternate allele dosage

	return($cgp);
}
# -----------------------------------------------------------------------------
# ==============================================================================

# Calculate Fst
# ==============================================================================
# original Hudson Fst=1-Hw/Hb, see SI in Bhatia et al. 2013, doi:10.1101/gr.154831.113
sub HudsonFst{
	my $af1=shift;
	my $af2=shift;
	my $numerator=$af1*(1-$af1)+$af2*(1-$af2); # Hw
	my $denominator=$af1*(1-$af2)+$af2*(1-$af1); # Hb
	my $fst=(1-$numerator/$denominator);
	return($fst,$numerator,$denominator);
}

# equation 10 in Bhatia et al. 2013, doi:10.1101/gr.154831.113
sub HudsonFstloci{
	my $af1=shift;
	my $af2=shift;
	my $n1=shift;
	my $n2=shift;
	my $numerator=($af1-$af2)**2-(($af1*(1-$af1))/($n1-1))-(($af2*(1-$af2))/($n2-1));
	my $denominator=$af1*(1-$af2)+$af2*(1-$af1);
	my $fst=$numerator/$denominator;
	return($fst,$numerator,$denominator);
}

# equation 6 in Bhatia et al. 2013, doi:10.1101/gr.154831.113
sub WeirCockerhamFst{
	my $af1=shift;
	my $af2=shift;
	my $n1=shift;
	my $n2=shift;
	my $numerator=(2*(($n1*$n2)/($n1+$n2))*(1/($n1+$n2-2)))*($n1*$af1*(1-$af1)+$n2*$af2*(1-$af2));
	my $denominator=(($n1*$n2)/($n1+$n2))*($af1-$af2)**2+(2*(($n1*$n2)/($n1+$n2))-1)*(1/($n1+$n2-2))*($n1*$af1*(1-$af1)+$n2*$af2*(1-$af2));
	my $fst=(1-$numerator/$denominator);
	return($fst,$numerator,$denominator);
}
# ==============================================================================

# Basic statistics
# ==============================================================================
sub mean{
	my $avg=0;
	foreach my $v (@_){
		$avg+=$v;
	}
	$avg/=scalar(@_);

	return($avg);
}

sub sum{
	my $s=0;
	foreach (@_){
		$s+=$_;
	}

	return($s)
}
# ==============================================================================

# Convert bcf to gl
# ==============================================================================
sub bcf2gl{
	my $in=shift;
	my $out=shift;
	my @genolhl=();
	my @af_bcf=();
	my @ids = ();
	my $readgendata=0; # read genetic data
	open (IN, "$in")
		or die "\nCan't read bcf file with $in\n\n";
		my $nsnps=0;
		while (<IN>){
			chomp;
			## get individual ids
			if (m/^#CHROM/){
				my @aux=split(m/\s+/, $_);
				foreach my $i (9..$#aux){
					$aux[$i]=~ s/(\.sorted)?(\.[s|b]am)?//g;
					push (@ids, $aux[$i]);
				}
				# print OUT join (" ", @ids)."\n";
				$readgendata=1;
				next;
			}
			## read genetic data lines, write gl
			elsif ($readgendata && (!m/[AGCT],[AGCT]/)){ # second condition discards multiallelic snps
				print "SNPs read from input file: $nsnps\r" if ($nsnps%100==0);
				$|++; # flush buffer

				my @aux=split(/\s+/,$_);
				my $id = "$aux[0]".":"."$aux[1]";
				my $line = "$id ";
				@aux = split(m/\s+/, $_);
				if ($aux[4]!~ m/\,/ &&    # exclude multiallelic SNPs (> 2 alleles)
					$aux[3] ne 'N'){      # exclude SNPs where ref = N
					my @afs=();
					while ($aux[7]=~ m/AF1\=([0-1]\.?[0-9]*e?\-?[0-9]*)\;/g){
						push (@afs, $1);
					}
					# Store last estimated allele frequency
					push (@af_bcf, $afs[$#afs]);

					foreach my $ax (@aux){
						if ($ax =~ m/^[0|1]\/[0|1]\:([0-9]+,[0-9]+,[0-9]+)\:/){
							$ax=$1;
							$ax =~ s/,/ /g;
							$line.=" $ax";
						}
						elsif ($ax =~ m/0\/0\:0\:/){ # invariant position
							$line.=" 1 0 0";
						}
					}
					push (@genolhl, $line);
				}
				$nsnps++;
			}
		}
	close (IN);
	open (OUT, "> $out")
		or die "\nCan't write to $out\n\n";

		print OUT scalar(@ids)." $nsnps\n";
		foreach my $g (@genolhl){
			print OUT "$g\n";
		}
	close (OUT);
	print "SNPs read from input file: $nsnps\r";
	print "\n\n";

	return (\@genolhl, \@af_bcf, \@ids);
}
# ==============================================================================

# Show copyright
# ==============================================================================
sub author{
    print "\n";
	print "#########################################\n";
	print "  ".basename($0)." version $version     \n";
    print "  (c) Victor Soria-Carrasco             \n";
    print "  victor.soria.carrasco\@gmail.com      \n";
	print "#########################################\n";
}
# ==============================================================================

# Show usage
# ==============================================================================
sub usage{
	my $txt='';
	$txt=shift if (scalar(@_)>0);
	print "\n";
    print "  Usage:\n";
    print "    ".basename($0)."\n";
    print "      -i    <SNPs input file (bcf/vcf format)>\n";
	print "      -p    <population pair file>\n";
	print "      -w    <genome windows file (optional)>\n";
    print "      -o    <output file>\n";
    print "      -ow   <output file for genome windows (optional)>\n";
    print "      -ms   <minimum sampling coverage> (optional, default=0.5)\n";
    print "      -mf   <minimum minor allele frequency> (optional, default=0.05)\n";
	print "      -pr   <0|1> prior for calculating empirical posterior probabilities of genotypes\n";
	print "            (0->uniform | 1->Hardy-Weinberg*; optional, default=0)\n";
	print "      -am   <0|1> allele frequency estimation method\n";
	print "            (0->using empirical Bayesian genotypes | 1->using EM algorithm*, default=0)\n";
	print "      -fm   <0|1|2> Fst estimation method\n";
	print "            (0->Hudson | 1->Hudson loci | 2->Weir & Cockerham; optional, default=0)\n";
	print "      -sep  <0|1> estimate allele frequency of each population separately\n";
	print "            (0->no | 1->yes; optional, default=0)\n";
	print "      -k    <0|1> keep temporary files\n";
	print "            (0->no | 1->yes; optional, default=0)\n";
    print "\n";
    print "      * Using a H-W prior will enable maximum-likelihood estimation of allele frequencies\n";
	print "        using the EM algorithm described in Li 2011 doi:10.1093/bioinformatics/btr509\n";
    print "\n";
	print "  Example:\n";
	print "      ".basename($0)." -i input.bcf -p poppairs.csv -o fst_loci.csv\n";
    print "\n\n";
	print $txt;
    exit;
}
# ==============================================================================



