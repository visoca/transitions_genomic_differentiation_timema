#!/usr/bin/perl
# (c) Zach Gompert zachariah.gompert@usu.edu
#
# sliding 20kb window analysis of neis Da, Dxy, pi, and Fst
# calculations for the reanalysis of Tcristinae whole genomes
#

$files = "filesTcr.txt";
$nloci = 9760626;
$rfilebase="sourceTcr";

$window = 20000;
open(IN, $files) or die "could not read the files\n";
<IN>; # discard header
$i = 0;
while(<IN>){
	chomp;
	print "$_ - $rfilebase$i.R\n";
	@f = split(",",$_);
	$out = $f[2];
	open (R, "> $rfilebase$i.R") or die "could not write the R file\n";
	print R "nl<-$nloci\nnc<-4\n";
    print R "llist<-matrix(scan(\"locuslist-tcris.txt\",n=nl*4,sep=\"\ \",skip=1),nrow=nl,ncol=4,byrow=T)\n";
	print R "scafs<-unique(llist[,3])\n";
	print R "p1<-matrix(scan(\"$f[0]\",n=nl*nc,what=\"character\",sep=\"\ \",skip=1),nrow=nl,ncol=nc,byrow=T)\n";
	print R "p2<-matrix(scan(\"$f[1]\",n=nl*nc,what=\"character\",sep=\"\ \",skip=1),nrow=nl,ncol=nc,byrow=T)\n";
	print R "pavg<-(as.numeric(p1[,4]) + as.numeric(p2[,4]))/2\n";
	print R "pi1<-2 * as.numeric(p1[,4]) * (1 - as.numeric(p1[,4]))\n";
	print R "pi2<-2 * as.numeric(p2[,4]) * (1 - as.numeric(p2[,4]))\n";
	print R "piavg<-2 * pavg * (1 - pavg)\n";
	print R "dxy<-as.numeric(p1[,4]) * (1 - as.numeric(p2[,4])) + (1 - as.numeric(p1[,4])) * as.numeric(p2[,4])\n";
	print R "header<-rbind(c(\"lg\",\"ord\",\"scaf\",\"midpoint\",\"snvs\",\"pi\",\"D\",\"Da\",\"fst\"))\n";
	print R "write.table(header,file=\"$out\",row.names=F,col.names=F,quote=F)\n";
	print R "for(i in 1:length(scafs)){## loop through scaffolds\n";
	print R "x<-which(llist[,3]==scafs[i]);pi1s<-pi1[x];pi2s<-pi2[x];dxys<-dxy[x];piavgs<-piavg[x]\n";
	print R "lls<-llist[x,4]\n";
	print R "window<-$window\n";
	print R "n<-length(lls);bnds<-seq(lls[1],lls[n],window)\n";
	print R "for(j in 1:(length(bnds)-1)){\n";
	print R "a<-which(lls >= bnds[j] & lls < bnds[j+1])\n";
	print R "if(length(a) >= 1){\n";
	print R "pi<-sum((pi1s[a]+pi2s[a])/2) * 1/window\n"; ## per nucleotide statistics
	print R "D<-sum(dxys[a]) * 1/window\n"; ## per nucleotide statistics
	print R "pit<-sum(piavgs[a]) * 1/window\n";
	print R "fst<-(pit - pi)/pit\n";
	print R "Da<-D-pi\n"; ## per nucleotide statistics
	print R "lg<-unique(llist[llist[,3]==scafs[i],1])\n";
	print R "ord<-unique(llist[llist[,3]==scafs[i],2])\n";
	print R "nei<-cbind(lg,ord,scafs[i],((bnds[j]+bnds[j+1])/2),length(a),pi,D,Da,fst)\n";
	print R "write.table(round(nei,8),file=\"$out\",row.names=F,col.names=F,quote=F,append=TRUE)\n";
	print R "}}}\n";

	close(R);
	$i++;
}
