#!/usr/bin/perl
# (c) Zach Gompert zachariah.gompert@usu.edu

# fit HMM to Da, Dxy, pi, and Fst windows

## makes 2 state hmm R scripts, give fst rwsp
@files=`ls nei20k_*`;
foreach $pp (@files){
    $pp =~ m/^nei20k_(\w+)/ or die "i cant match that shit\n";
    $id = $1;
    print "got one ... $id\n";

    open (R, "> fitHmm2"."$id".".R") or die "fuck the what?";

    print R "library(HiddenMarkov)\n";

    print R "nl<-42687\n";

    print R "nei<-matrix(scan(\"$pp\",n=nl*9,sep=\" \"),nrow=nl,ncol=9,byrow=TRUE)\n";


    print R "Mstep.normc <- function(x, cond, pm, pn){\nnms <- sort(names(pm))\n
         n <- length(x)\n
         m <- ncol(cond\$u)\n
         if (all(nms==c(\"mean\", \"sd\"))){\n
             mean <- pm\$mean\n
             #mean <- as.numeric(matrix(x, nrow = 1) %*% cond\$u)/apply(cond\$u,
             #MARGIN = 2, FUN = sum)\n
             sd <- pm\$sd\n
             return(list(mean=mean, sd=sd))\n
         }\n
         if (all(nms == \"mean\")) {\n
             mean <- pm\$mean\n
             #mean <- as.numeric(matrix(x, nrow = 1) %*% cond$u)/apply(cond\$u,
             #MARGIN = 2, FUN = sum)\n
             return(list(mean = mean))\n
         }\n
         if (all(nms == \"sd\")) {\n
             sd <- pm\$sd\n
             return(list(sd = sd))\n
         }\n
         stop(\"Invalid specification of parameters\")\n

     }\n

rnormc <- rnorm\n
dnormc <- dnorm\n
pnormc <- pnorm\n
qnormc <- qnorm\n";

    print R "lgwin<-which(is.na(nei[,1])==FALSE)\n";
    print R "for(parm in 9){\n";
    #print R "for(parm in 4:7){\n";
    print R "if(parm < 9){lparm<-log(nei[lgwin,parm])\n";
    print R "lparm[is.finite(lparm)==FALSE]<-log(0.000001)\n}\n";
    print R "else{lparm<-log(nei[lgwin,parm]/(1-nei[lgwin,parm]))\n";
    print R "lparm[is.finite(lparm)==FALSE]<-log(0.000001/0.999999)\n}\n";
    print R "nl<-length(lgwin)\n";

print R "delta<-c(0.05,0.95)\n
PiA<-matrix(c(0.9,0.1,0.1,0.9),nrow=2,byrow=T)\n
PiB<-matrix(c(0.8,0.2,0.01,0.99),nrow=2,byrow=T)\n
Pi<-list(PiA,PiB)\n

mn<-mean(lparm,na.rm=TRUE)\n
s<-sd(lparm,na.rm=TRUE)\n
q90<-quantile(lparm,prob=0.9)\n
params<-vector(\"list\",2)\n
fit<-vector(\"list\",2)\n

for(i in 1:2){\n

        init<-dthmm(lparm,Pi[[i]],delta,\"normc\",list(mean=c(q90,mn),sd=rep(s,2)),discrete=FALSE)\n
        params[[i]]<-BaumWelch(init,bwcontrol(maxiter = 500, tol = 1e-04, prt = TRUE, posdiff = FALSE,
               converge = expression(diff < tol)))\n
        fit[[i]]<-Viterbi(params[[i]])\n
}
save(list=ls(),file=paste(\"hmm2s_"."$id"."par\",parm,\".rwsp\",sep=\"\"))\n";

    print R "mycolors<-c(\"orange\",\"darkgray\")\n
png(file=paste(\"hmm2stPlot"."$id"."par\",parm,\".png\",sep=\"\"),width=2400,height=800)\n
if(parm < 9){eparm<-exp(lparm)}\n
else{eparm<-1/(1+exp(-1 * lparm))}\n
plot(eparm,col=mycolors[fit[[1]]],pch=19,ylim=c(0,1),xlab=\"snp number\",ylab=\"Fst\",cex.lab=1.5,cex.axis=1.2,main=\"\")\n
lgs<-nei[lgwin,1]
bnds<-which(lgs[1:(nl-1)] != lgs[2:nl]) + 0.5
abline(v=bnds,lty=2)
dev.off()\n
if(parm < 9){out<-round(rbind(exp(params[[1]]\$pm\$mean),table(fit[[1]])/length(fit[[1]]),params[[1]]\$Pi),4)}\n
else{out<-round(rbind(1/(1+ exp(-1*params[[1]]\$pm\$mean)),table(fit[[1]])/length(fit[[1]]),params[[1]]\$Pi),4)}\n
write.table(out,file=paste(\"hmm2stSummary"."$id"."par\",parm,\".txt\",sep=\"\"),row.names=F,col.names=F)\n
cor.test(fit[[1]],fit[[2]])\n
}\n
rm(list=ls())\n";
    close(R);
}
