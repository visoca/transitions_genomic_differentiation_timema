#!/usr/bin/env perl

# (c) Victor Soria-Carrasco
# victor.soria.carrasco@gmail.com
# Last modified: 16/08/2016 18:18:39

# Description:
# Converts a bcf file to a multiple aligment in fasta format
# See arguments below.

# Changelog
# 1.3 - 2017/02/24
#   - fixed serious bug that may cause loci to be written out in
#     different order for different samples
#
# 1.2 - 2016/08/02
#	- added option for multistate output

use warnings;
use strict;
use File::Basename;
use File::Spec qw(rel2abs);
use Getopt::Long;
use Text::Wrap;
$Text::Wrap::columns=80;

my $version='1.3-2017.02.24';

&author;

# External programs:

my $bcftools='bcftools';

my $bcftools_version=`$bcftools |& grep Version | awk '{print \$2}'`;

my $ifile; # input file
my $ofile; # output file
my $rllist; # list to rename loci
my $rslist; # list to rename samples
my $maxpgaps='1.0'; # proportion of gaps per site allowed
my $maxphets='1.0'; # proportion of heterozyogotes per site allowed
my $maxpgaphets='1.0'; # proportion of heterozyogotes per site allowed

my $maxpgapt='1.0'; # proportion of gaps per taxa allowed
my $maxphett='1.0'; # proportion of heterozyogotes per taxa allowed
my $maxpgaphett='1.0'; # proportion of heterozyogotes per taxa allowed

my $partition=0; # partition by locus
my $multistate=0; # output multistate instead
my $consensus=0; # make consensus pooling multiple samples together
my $consfield=0; # field on samples names to pool samples together
my $conssplchr='-'; # character separating fields

GetOptions(
    'i|I=s'       => \$ifile,
    'o|O=s'       => \$ofile,
	'p|P'         => \$partition,
	'm|M'         => \$multistate,
	'c|C'         => \$consensus,
	'cf|CF=i'     => \$consfield,
	'rl|RL=s'     => \$rllist,
	'rs|RS=s'     => \$rslist,
	'pgs|PGS=f'   => \$maxpgaps,
	'phs|PHS=f'   => \$maxphets,
	'pghs|PGHS=f' => \$maxpgaphets,
    'pgt|PGT=f'   => \$maxpgapt,
	'pht|PHT=f'   => \$maxphett,
	'pght|PGHT=f' => \$maxpgaphett,
	'h|help'      => \&usage
);

&usage if	(!defined($ifile) ||
			($partition==1 && (!defined($ofile))) ||
			($consensus==1 && (!defined($rslist)))) ;

$ifile=File::Spec->rel2abs($ifile);
my $input=$ifile;
if ($input =~ m/\.bcf$/i){
	if ($bcftools_version=~ m/^0\.1/){ # bcftools 0.1.x
		$input="$bcftools view -N $ifile |";
	}
	else{ # bcftools v1.x
		$input="$bcftools view -e 'REF=\"N\"' $ifile |";
	}
}
elsif ($input =~ m/\.vcf/i){
	$input="cat $ifile |";
}
else{
	die ("\nFormat of input file $ifile not recognised\n\n");
}


# Read list of loci
my %rll;
if (defined($rllist)){
	open (FILE, "$rllist")
		or die ("\nCan't open file $rllist\n\n");
		while (<FILE>){
			chomp;
			my @aux=split(/\s|\,/,$_);
			$rll{$aux[0]}=$aux[1];
		}
	close (FILE);
}

# Read list of samples
my %rsl;
if (defined($rslist)){
	open (FILE, "$rslist")
		or die ("\nCan't open file $rslist\n\n");
		while (<FILE>){
			chomp;
			my @aux=split(/\s|\,/,$_);
			$rsl{$aux[0]}=$aux[1];
		}
	close (FILE);
}

my %seqs;
my @ids;
my $n=0;
my $c=0;
open (FILE, $input);
	while (<FILE>){
		if (/^\#/){ # header
			if (/CHROM/){
				chomp;
				my @aux=split(/\s/,$_);
				@ids=@aux[9..$#aux];
				if (defined($rslist)){
					foreach my $id (@ids){
						$id=~ s/(\.sorted)?(\.bam)?$//g;
						$id=$rsl{$id}."-".$id;
					}
				}
			}
		}
		else{
			$n++;
			chomp;
			my @aux=split(/\t/,$_);
			my @alleles;
			my $locus=$aux[0];
			$locus=$rll{$locus} if (defined($rllist));

			$alleles[0]=$aux[3];
			push (@alleles, split(/\,/,$aux[4]));
			my $pgaps=0;
			my $phets=0;
			for my $i (9..$#aux){ # samples
				# gaps
				if (($aux[$i]=~ m/\:(0\,0\,0)+/) || # no data or very little dat, coded as GL or PL = 0,0,0
					 $aux[$i]=~ m/^0$/ || # special case for bcftools 0.1x, no data coded as a single 0
					 $aux[$i]=~ m/^\.\/?\.?\:/){ # generic case (for bcftools 1.x, freebayes, etc)
						$seqs{$ids[$i-9]}{$locus}.='-';
						$pgaps++;
				}
				elsif ($aux[$i]=~ m/^0\/0\:/ || $aux[$i]=~ m/^1\/1\:/){
					my $al=substr($aux[$i],0,1);
					$seqs{$ids[$i-9]}{$locus}.=$alleles[$al];
				}
				else{
					my $het=code_heterozygote($alleles[0].$alleles[1]);
					$seqs{$ids[$i-9]}{$locus}.=$het;
					$phets++;
				}
			}
			if ($maxpgaps <1 || $maxphets < 1 || $maxpgaphets < 1){
				# Calculate combined proportion of gaps and heterozygotes for this site
				my $pgaphets=($pgaps+$phets)/(scalar(@aux)-9);
				# Calculate proportion of gaps for this site
				$pgaps=$pgaps/(scalar(@aux)-9);
				# Calculate proportion of heterozygotes for this site
				$phets=$phets/(scalar(@aux)-9);
				if ($pgaps > $maxpgaps || $phets > $maxphets || $pgaphets > $maxpgaphets){
					for my $i (9..$#aux){
						chop($seqs{$ids[$i-9]}{$locus});
					}
				}
				else{
					$c++;
				}
			}
			else{
				$c++;
			}
			print STDERR " # SNPs retained: $c out of $n\r" if ($n%1000==0 || eof);
		}
	}
close (FILE);

print STDERR " # SNPs retained: $c out of $n\r";
print STDERR "\n\n";



my @loci=sort keys %{$seqs{$ids[0]}};


# Do consensus
# ------------------------------------------------------------------------------
if ($consensus==1){
	# create arrays pooling samples
	my %taxa;
	foreach my $id (sort keys %seqs){
		my @aux=split(/$conssplchr/,$id);
		# print "Adding $id to $aux[$consfield]\n";
		push(@{$taxa{$aux[$consfield]}},$id);
	}
	# compute consensus sequence for each taxon (= pooled seqs)
	my %newseqs;
	foreach my $t (sort keys %taxa){
		# print "T: $t\n";
		foreach my $l (@loci){
			my @seqs=();
			foreach my $id (@{$taxa{$t}}){
				push (@seqs, $seqs{$id}{$l});
				$|++;
			}
			$newseqs{$t}{$l}=consensus(@seqs);
		}

	}
	%seqs=%newseqs;
}
# ------------------------------------------------------------------------------


# Output
# ------------------------------------------------------------------------------
my %locus_files;
if (defined($ofile)){
	if ($partition==0){
		open (FILEOUT, ">$ofile")
			or die "(\nCan't write to output file $ofile\n\n)";
	}
	else{
		# open a file for each locus
		foreach my $l (@loci){
			my $of=$ofile;
			$of=~ s/\.fa(sta)?//g;
			$of.="-$l.fa";
			open (my $fh, "> $of")
				or die ("\nCan't write to $of\n\n");
			$locus_files{$l}=$fh;
		}
	}
}
else{
	*FILEOUT = *STDOUT;
}

if ($maxpgapt < 1 || $maxphett < 1 || $maxpgaphett < 1){
	foreach my $id (sort keys %seqs){
		# Calculate proportion of gaps and heterozygotes for each sample/taxon
		# Concatenate all loci
		my $seq='';
		foreach my $l (sort keys %{$seqs{$id}}){
			$seq.=$seqs{$id}{$l};
		}

		my $pgaphett=0;

		# Calculate proportion of gaps for this sample/taxon
		my $pgapt=()=$seq =~ /\-/g;
		$pgaphett+=$pgapt;
		$pgapt/=length($seq);

		# Calculate proportion of heterozygotes for this sample/taxon
		my $phett=()=$seq=~ /[KMRSWY]/ig;
		$pgaphett+=$phett;
		$phett/=length($seq);

		# Calculate combined proportion of gaps and heterozygotes for this sample/taxon
		$pgaphett/=length($seq);

		if ($pgapt <= $maxpgapt && $phett <= $maxphett && $pgaphett <= $maxpgaphett){
			my $outid=$id;
			$outid=~ s/(\.sorted)?\.bam//g; # remove extensions
			if ($partition==0){
				print FILEOUT ">$outid\n";
				$seq=code_multistate($seq) if ($multistate==1);
				print FILEOUT wrap('','', $seq."\n");
			}
			else{
				#Output each locus to a separate file
				foreach my $l (sort keys %{$seqs{$id}}){
					print {$locus_files{$l}} ">$outid\n";
					$seqs{$id}{$l}=code_multistate($seqs{$id}{$l}) if ($multistate==1);
					print {$locus_files{$l}} wrap('','', $seqs{$id}{$l}."\n");
				}
			}
		}
		else{
			print STDERR "$id excluded (pgap: $pgapt > $maxpgapt or phet: $phett > $maxphett, or pgaphet: $pgaphett > $maxpgaphett)\n";
		}
	}
}
else{
	foreach my $id (sort keys %seqs){
		my $outid=$id;
		$outid=~ s/(\.sorted)?\.bam//g; # remove extensions

		if ($partition==0){
			# Concatenate all loci
			my $seq='';
			foreach my $l (sort keys %{$seqs{$id}}){
				$seq.=$seqs{$id}{$l};
			}
			print FILEOUT ">$outid\n";
			$seq=code_multistate($seq) if ($multistate==1);
			print FILEOUT wrap('','', $seq."\n");
		}
		else{
			#Output each locus to a separate file
			foreach my $l (sort keys %{$seqs{$id}}){
				print {$locus_files{$l}} ">$outid\n";
				$seqs{$id}{$l}=code_multistate($seqs{$id}{$l}) if ($multistate==1);
				print {$locus_files{$l}} wrap('','', $seqs{$id}{$l}."\n");
			}
		}
	}
}

if ($partition==0){
	close (FILEOUT) if (defined($ofile));
}
else{
	foreach my $l (@loci){
		close ($locus_files{$l});
	}
}
# ------------------------------------------------------------------------------

# ==============================================================================
# ==============================================================================
# ============================== SUBROUTINES ===================================
# ==============================================================================
# ==============================================================================


# Show copyright
# ==============================================================================
sub author{
    print "\n";
    print "#########################################\n";
    print "  ".basename($0)."\n";
	print "  version $version     \n";
    print "  (c) Victor Soria-Carrasco             \n";
    print "  victor.soria.carrasco\@gmail.com      \n";
    print "#########################################\n";
	print "\n";
}
# ==============================================================================

# Show usage
# ==============================================================================
sub usage{
    print "\n";
	print "  Usage:\n";
    print "    ".basename($0)."\n";
	print "      -i <input file>\n";
	print "      -o <output file>\n";
	print "      -p (output each locus in a separate file; requires specifying an output file)\n";
	print "      -m (output multistate encoding)\n";
	print "      -c (make consensus pooling multiple samples; requires specifying a list of samples with -rs )\n";
	print "      -rl   <csv file> (rename loci using this list)\n";
	print "      -rs   <csv file> (rename samples using this list)\n";
	print "      -pgs  <proportion of gaps per site allowed (optional; 0-1; default:1.0)>\n";
	print "      -phs  <proportion of heterozygotes per site allowed (optional; 0-1; default:1.0)>\n";
	print "      -pghs <combined proportion of gaps and heterozygotes per site allowed (optional; 0-1; default:1.0)>\n";
   	print "      -pgt  <proportion of gaps per taxa allowed (optional; 0-1; default:1.0)>\n";
	print "      -pht  <proportion of heterozygotes per taxa allowed (optional; 0-1; default:1.0)>\n";
	print "      -pght <combined proportion of gaps and heterozygotes per taxa allowed (optional; 0-1; default:1.0)>\n";
    print "\n";
    exit;
}
# ==============================================================================

# Code heterozygotes according to IUPAC
# ==============================================================================
sub code_heterozygote{
	my %het=(
		'AC'=>'M',
		'AG'=>'R',
		'AT'=>'W',
		'CA'=>'M',
		'CG'=>'S',
		'CT'=>'Y',
		'GA'=>'R',
		'GC'=>'S',
		'GT'=>'K',
		'TA'=>'W',
		'TC'=>'Y',
		'TG'=>'K'
	);

	return ($het{$_[0]});
}
# ==============================================================================

# Resolve heterozygotes encoded according to IUPAC
# ==============================================================================
sub rev_code_heterozygote{
	my %revhet=(
		'M'=>'AC',
		'R'=>'AG',
		'W'=>'AT',
		'S'=>'CG',
		'Y'=>'CT',
		'K'=>'GT'
	);
	my @out=split('',$revhet{$_[0]});
	return (@out);
}
# ==============================================================================

# IUPAC ambiguities
# ==============================================================================
sub code_ambiguities{
	my %amb=(
		'AC'=>'M', 'CA'=>'M',
		'AG'=>'R', 'GA'=>'R',
		'AT'=>'W', 'TA'=>'W',
		'CG'=>'S', 'GC'=>'S',
		'CT'=>'Y', 'TC'=>'Y',
		'GT'=>'K', 'TG'=>'K',
		'ACG'=>'V',	'AGC'=>'V',	'CGA'=>'V',	'CAG'=>'V',	'GAC'=>'V',	'GCA'=>'V',
		'ACT'=>'H',	'ATC'=>'H',	'CTA'=>'H',	'CAT'=>'H',	'TAC'=>'H',	'TCA'=>'H',
		'AGT'=>'D',	'ATG'=>'D',	'GTA'=>'D',	'GAT'=>'D',	'TAG'=>'D',	'TGA'=>'D',
		'CGT'=>'B',	'CTG'=>'B',	'GTC'=>'B',	'GCT'=>'B',	'TCG'=>'B',	'TGC'=>'B',
		'ACGT'=>'N', 'ACTG'=>'N', 'ATCG'=>'N', 'TACG'=>'N', 'TAGC'=>'N', 'ATGC'=>'N',
		'AGTC'=>'N', 'AGCT'=>'N', 'GACT'=>'N', 'GATC'=>'N', 'GTAC'=>'N', 'TGAC'=>'N',
		'TGCA'=>'N', 'GTCA'=>'N', 'GCTA'=>'N', 'GCAT'=>'N', 'CGAT'=>'N', 'CGTA'=>'N',
		'CTGA'=>'N', 'TCGA'=>'N', 'TCAG'=>'N', 'CTAG'=>'N', 'CATG'=>'N', 'CAGT'=>'N'
	);

	return ($amb{$_[0]});
}
# ==============================================================================

# Code multistate for RAxML
# ==============================================================================
sub code_multistate{
	my $seq=shift;
	my @aux=split(//,$seq);
	my %state=(
		'A'=>'0',
		'C'=>'1',
		'T'=>'2',
		'G'=>'3',
		'M'=>'4',
		'R'=>'5',
		'W'=>'6',
		'S'=>'7',
		'Y'=>'8',
		'K'=>'9',
		'V'=>'A',
		'H'=>'B',
		'D'=>'C',
		'B'=>'D',
		'N'=>'E'
	);

	$seq='';
	foreach my $a (@aux){
		if (defined($state{$a})){
			$seq.=$state{$a};
		}
		else{
			$seq.=$a;
		}
	}

	return ($seq);
}
# ==============================================================================


# Do consensus
# ==============================================================================
sub consensus{
	my @ali=@_;
	my $conseq='';
	my $thr=0.5; # threshold to use ambiguities (NOT USED)
	while (length($ali[0]) > 0){
		my %cnt;
		my $totcnt=0;
		foreach my $s (@ali){
			my $nt=substr ($s, 0, 1, "");
			$nt=uc($nt);
			if ($nt !~ /[A|T|C|G|-]/){

				my @nts=rev_code_heterozygote($nt);
				foreach my $n (@nts){
					$cnt{$n}++;
				}
			}
			else{
				$cnt{$nt}+=2;
			}
			$totcnt+=2;
		}
		my $max=0;
		my $fnt='-';
		foreach my $nt (keys %cnt){
			if ($nt ne '-' && $cnt{$nt} > $max){
				$fnt=$nt;
				$max=$cnt{$nt};
			}
			elsif ($nt ne '-' && $cnt{$nt} == $max){
				$fnt=code_heterozygote($fnt.$nt);
			}
		}
		$conseq.=$fnt;

	}
	return($conseq);
}
# ==============================================================================


