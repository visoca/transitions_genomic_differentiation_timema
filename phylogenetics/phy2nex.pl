#!/usr/bin/env perl

# (c) Victor Soria-Carrasco
# victor.soria.carrasco@gmail.com
# Last modified: 14/08/2014 01:06:32

# Description:
# Converts a multiple alignment from phylip to nexus format
#
# Usage:
# phy2nex.pl <phylip file>

use warnings;
use strict;
use File::Basename;

my $version='1.0-2014.08.14';

&author;

&usage if (scalar(@ARGV)<1 || $ARGV[0]=~ m/-h/i);

my $input=$ARGV[0];
my $output=$input;
my $format=0;
if (defined($ARGV[1]) && $ARGV[1] eq '-m'){
	$format=1;
}
$output=~ s/\.phy(lip)*.*/\.nex/g;

my $ntaxa=0;
my $nsites=0;
my %seqs;
my $id="";
my $maxlen=0;
open (FILE, $input);
	my $first=(<FILE>); #first line
	$first=~ s/^\s//g;
	chomp($first);
	my @aux=split(/\s/,$first);
	$ntaxa=$aux[0];
	$nsites=$aux[1];
	while (<FILE>){
		next if (/^\s*\n$/);
		chomp;
		if (!/^\s/){
			my @aux=split(/\s/,$_);
			$id=$aux[0];
			$maxlen=length($id) if (length($id)>$maxlen);
			foreach my $i (1..$#aux){
				$seqs{$id}.=$aux[$i];
			}
		}
		else{
			s/\s//g;
			$seqs{$id}.=$_;
		}
	}
close (FILE);

# Check length
my $len=-1;
foreach my $s (keys %seqs){
	if ($len >-1){
		die "This is not a multiple alignment: sequences have different lenghts\n\n"
			if ($len != length($seqs{$s}));
	}
	$len=length($seqs{$s});
}

$nsites=$len;

open (FILEOUT, ">$output");
	print FILEOUT '#NEXUS'."\n";
	print FILEOUT "Begin data;\n";
	print FILEOUT "\tDimensions ntax=$ntaxa nchar=$nsites;\n";

	if ($format==0){
		print FILEOUT "\tFormat datatype=dna symbols='ACTGKMRSWY' gap=- missing=-;\n";
	}
	elsif ($format==1){
		print FILEOUT "\tFormat datatype=standard symbols='0123456789' gap=- missing=-;\n";
	}

	print FILEOUT "\tMatrix\n";
	foreach my $s (sort keys %seqs){
		printf FILEOUT ("%-".($maxlen+1)."s ", $s);
		print FILEOUT $seqs{$s}."\n";
	}
	print FILEOUT "\t;\n";
	print FILEOUT "End;\n";
close (FILEOUT);

print "Output file saved to: $output\n\n";

# ==============================================================================
# ==============================================================================
# ============================== SUBROUTINES ===================================
# ==============================================================================
# ==============================================================================

# Show copyright
# ==============================================================================
sub author{
    print "\n";
    print "#########################################\n";
    print "  ".basename($0)." version $version     \n";
    print "  (c) Victor Soria-Carrasco             \n";
    print "  victor.soria.carrasco\@gmail.com      \n";
    print "#########################################\n";
	print "\n";
}
# ==============================================================================

# Show usage
# ==============================================================================
sub usage{
    print "\n";
	print "  Usage:\n\n";
    print "    ".basename($0)." <input file (phylip format)> -m (optional, multistate)\n";
    print "\n";
    exit;
}
# ==============================================================================
