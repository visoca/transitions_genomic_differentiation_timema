#!/usr/bin/env perl

# (c) Victor Soria-Carrasco
# victor.soria.carrasco@gmail.com
# Last modified: 17/09/2015 19:02:22

# Description:
# This script calculates initial parameters for entropy analyses
# and generates a submission script for Iceberg

# WARNING: Beware this script may need large amounts of memory to run (R code)

use warnings;
use strict;
use Getopt::Long;
use File::Basename;
use File::Path qw(make_path);
use File::Spec;
use Statistics::R;
use POSIX qw(strftime);

my $date=strftime("%Y%m%d%-H%M%S", localtime);

my $version='1.0-2015.09.17';
my $ncpu=1;
my $bcf2geno='/usr/local/extras/Genomics/scripts/tools/bcf2bbgeno.pl';
my $bcf2gl='/usr/local/extras/Genomics/scripts/tools/bcf2gl.pl';

# Entropy default arguments
my $entropy='entropy';
my $estpost='estpost';
my $burnin=10000;
my $mcmclen=35000;
my $smpfreq=10;
my $mink=2;
my $maxk=3;

# SGE arguments
my $hrs=24;
my $mem=4;
my $queue='iceberg';
my $email='';
my $nruns=3;
my $node=`hostname`;
chomp($node);

&author;
my ($infile, $outdir);
GetOptions(
    'i|I=s'  => \$infile,
    'o|O=s'  => \$outdir,
    'mink=i' => \$mink,
    'maxk=i' => \$maxk,
    'n|N=i'  => \$nruns,
    'burn=i' => \$burnin,
    'mcmc=i' => \$mcmclen,
    'smfr=i' => \$smpfreq,
    'hrs=i'  => \$hrs,
    'q|Q=s'  => \$queue,
    'm|M=i'  => \$mem,
    'e|E=s'  => \$email,
    'h|help' => \&usage
);

&usage if (!defined($infile) || !defined($outdir));
my $nks=$maxk-$mink+1;

# SGE options
my $vmem=$mem+2;
$mem=$mem.'G';
$vmem=$vmem.'G';
my $njobs=$nruns*$nks;

# Input file
if (! -e $infile){
	die ("\nCan't access input file: $infile\n\n");
}
$infile=File::Spec->rel2abs($infile);


# Output directory
if (! -e $outdir){
	eval {make_path($outdir)}
		or die ("\nCan't create output directory: $outdir\n\n");
}
$outdir=File::Spec->rel2abs($outdir);

# Output files
my $outbase="$outdir/".basename($infile);
$outbase=~ s/\.[b|v]cf//g;

my $geno="$outdir/".basename($infile);
$geno=~ s/\.[b|v]cf/\.bbgeno/g;

my $gl="$outdir/".basename($infile);
$gl=~ s/\.[b|v]cf/\.gl/g;


my $smsjob="$outdir/entropy_".basename($infile).".$date.smsjob.sh";
my $smslog="$outdir/entropy_".basename($infile).".$date.smsjob.log";

# Calculate genotype probabilities
print "Calculating genotype probabilities and generating bimbam genotype file...\n\n";
system("$bcf2geno -i $infile -o $geno -p H-W -s >& /dev/null");

# Convert to gl format
print "Converting bcf/vcf file to genotype likelihood (gl) format...\n\n";
system("$bcf2gl -i $infile -o $gl -e >& /dev/null");

# Get number of loci and samples from bimbam genotype file
my $nloci=`cat $geno | wc -l`;
chomp($nloci);
my $nsamples=`sed '1q;d' $geno | awk '{print NF-3}'`;
chomp($nsamples);


# R code - Compute PCA and linear discriminant analysis
# --------------------------------------------------------------------------------------------
my $R = Statistics::R->new();

# Using covariance matrix
$R->run(qq`
        library(MASS)
        g<-matrix(
            scan(pipe("cut -d' ' -f4- $geno"),
				n=($nloci * $nsamples), sep=" "),
            nrow=$nloci, ncol=$nsamples, byrow=T)
		gmn<-matrix(apply(g,2,mean),nrow=$nloci,ncol=$nsamples,byrow=T)
		gprime<-(g-gmn) ## remove mean
		gcov<-cov(gprime)
		pcg<-prcomp(gcov,center=TRUE,scale=FALSE)`);

for my $k ($mink..$maxk){
	my $id='k'.$k;
	$R->run(qq`
			$id<-kmeans(pcg\$x[,1:5],$k,iter.max=10,nstart=10,algorithm="Hartigan-Wong")
			lda$id<-lda(x=pcg\$x[,1:5],grouping=$id\$cluster,CV=TRUE)
			write.table(round(lda$id\$posterior,5),
					file="$outbase-lda$id.txt",quote=F,row.names=F,col.names=F)`);
}

$R->stop();
# --------------------------------------------------------------------------------------------

# Now write down the entropy submission script
open (FILE, ">$smsjob")
	or die ("\nCan't open $smsjob\n\n");

print FILE <<EOF;
#!/bin/bash
#\$ -l h_rt=$hrs:00:00
#\$ -l rmem=$mem
#\$ -l mem=$vmem
#\$ -j y
#\$ -o $smslog
#\$ -t 1-$njobs
EOF
if ($email ne ''){
	print FILE '#$ -m bea'."\n";
	print FILE '#$ -M '.$email."\n";
}
if ($queue eq 'popgenom' || $queue eq 'molecol'){
	print FILE '#$ -P '.$queue."\n";
	print FILE '#$ -q '.$queue.".q\n";
}
else{
	print FILE '#$ -l scratch=190G'."\n";
}

# Change output dir to /local temporarily if using popgenom
if ($queue eq 'popgenom' || $queue eq 'molecol'){
	print FILE "\n".'mkdir "/local/$USER-'.$date.'"'.' >& /dev/null'."\n";
	print FILE "\n".'OUTDIR="/local/$USER-'.$date.'"'."\n";
	$node='node096';
}
else{
	print FILE "\n".'mkdir /scratch/$USER-'.$date.' >& /dev/null'."\n";
	print FILE "\n".'OUTDIR="/scratch/$USER-'.$date.'"'."\n";
}

# Set seeds for all runs
my $seeds='';
my $s1=my $s2=-1;
foreach my $i (1..$njobs){
	while ($s1 == $s2){
		$s2=int(rand(100000000));
	}
	$s1=$s2;
	$seeds.=$s2.' ';
}
$seeds=~ s/\ $//g;

print FILE <<EOF;

GL=$gl
GLNAME=\$(basename \$GL)
K=\$((\$((SGE_TASK_ID-1)) / $nruns + $mink))
RUNINDEX=\$((\$((SGE_TASK_ID-1)) % $nruns + 1))

# echo GL: \$GL - K: \$K - RUNINDEX: \$RUNINDEX

OUTFILE="\$OUTDIR/\${GLNAME%.*}_k\$K"_"\$RUNINDEX.hdf5"
QOUTPOST="\$OUTDIR/\${GLNAME%.*}_k\$K"_"\$RUNINDEX.q.postout.txt"
GOUTPOST="\$OUTDIR/\${GLNAME%.*}_k\$K"_"\$RUNINDEX.gprob.postout.txt"
IDLOG="\${GL%.*}_k\$K"_"\$RUNINDEX.log"

echo "Array job ID: \$SGE_TASK_ID" > \$IDLOG
SEED=($seeds)
echo

# echo \$OUTFILE - \$OUTPOST - \$IDLOG

hostname >> \$IDLOG
uname -a >> \$IDLOG
date >> \$IDLOG
echo "---------------------------------------------------" >> \$IDLOG
echo >> \$IDLOG
echo "Running entropy for \$GL..." >> \$IDLOG
echo >> \$IDLOG
echo "CMD: " >> \$IDLOG
echo "    $entropy \\\\" >> \$IDLOG
echo "        -i \$GL \\\\" >> \$IDLOG
echo "        -m 1 -w 1 \\\\" >> \$IDLOG
echo "        -r \${SEED[\$((SGE_TASK_ID-1))]} \\\\" >> \$IDLOG
echo "        -b $burnin \\\\">> \$IDLOG
echo "        -l $mcmclen \\\\">> \$IDLOG
echo "        -t $smpfreq \\\\">> \$IDLOG
echo "        -k \$K \\\\">> \$IDLOG
echo "        -q $outbase-ldak\$K.txt \\\\">> \$IDLOG
echo "        -s 50 \\\\">> \$IDLOG
echo "        -o \$OUTFILE" >> \$IDLOG
echo >> \$IDLOG
echo "    $estpost \\\\" >> \$IDLOG
echo "        -c 0.95 \\\\" >> \$IDLOG
echo "        -p q \\\\">> \$IDLOG
echo "        -s 0 \\\\">> \$IDLOG
echo "        -o \$QOUTPOST \\\\">> \$IDLOG
echo "        \$OUTFILE" >> \$IDLOG
echo >> \$IDLOG
echo "    $estpost \\\\" >> \$IDLOG
echo "        -c 0.95 \\\\" >> \$IDLOG
echo "        -p gprob \\\\">> \$IDLOG
echo "        -s 0 \\\\">> \$IDLOG
echo "        -o \$GOUTPOST \\\\">> \$IDLOG
echo "        \$OUTFILE" >> \$IDLOG
echo >> \$IDLOG
echo "---------------------------------------------------" >> \$IDLOG
echo >> \$IDLOG

$entropy \\
-i \$GL \\
-m 1 -w 1 \\
-r \${SEED[\$((SGE_TASK_ID-1))]} \\
-b $burnin \\
-l $mcmclen \\
-t $smpfreq \\
-k \$K \\
-q $outbase-ldak\$K.txt \\
-s 50 \\
-o \$OUTFILE  >> \$IDLOG 2>&1

$estpost \\
-c 0.95 \\
-p q \\
-s 0 \\
-o \$QOUTPOST \\
\$OUTFILE >> \$IDLOG 2>&1

$estpost \\
-c 0.95 \\
-p gprob \\
-s 0 \\
-o \$GOUTPOST \\
\$OUTFILE >> \$IDLOG 2>&1

# Some resilience to avoid frequent fastdata problems
while [ ! -f "$outdir/\$(basename \$OUTFILE)" ];
do
	cp \$OUTFILE $outdir
	cp \$QOUTPOST $outdir
	cp \$GOUTPOST $outdir
	sleep \$((30+\$RANDOM%90)) # wait 30-120 secs to try again
done

rm \$OUTFILE

echo >> \$IDLOG
echo "---------------------------------------------------" >> \$IDLOG
echo >> \$IDLOG
date >> \$IDLOG
echo >> \$IDLOG


EOF

close (FILE);

system("chmod +x $smsjob");

print "Command to submit the job to Iceberg ($queue queue):\n\n";
print "qsub $smsjob\n\n";

# ==============================================================================
# ==============================================================================
# ============================== SUBROUTINES ===================================
# ==============================================================================
# ==============================================================================

# Show copyright
# ==============================================================================
sub author{
    print "\n";
    print "#########################################\n";
    print "  ".basename($0)."\n";
	print "  version $version\n";
    print "  (c) Victor Soria-Carrasco             \n";
    print "  victor.soria.carrasco\@gmail.com      \n";
    print "#########################################\n";
	print "\n";
}
# ==============================================================================

# Show usage
# ==============================================================================
sub usage{
    print "\n";
	print "  Usage:\n";
    print "    ".basename($0)."\n";
	print "      -i <input file (bcf file)>\n";
	print "      -o <output directory>\n";
	print "      -mink <min. K (optional, default=2)>\n";
	print "      -maxk <max. K (optional, default=3)>\n";
	print "      -n <no. of runs (optional, default=3)>\n";
	print "      -burn <MCMC burnin (optional, default=10000)>\n";
	print "      -mcmc <MCMC length (optional, default=35000)>\n";
	print "      -smfr <MCMC sampling frequency (optional, default=10)>\n";
	print "      -hrs <max. time allocated in hours (optional, default=24)>\n";
	print "      -q <queue (iceberg|popgenom|molecol, optional, default=iceberg)>\n";
	print "      -m <memory (optional, default=4)>\n";
	print "      -e <email (optional, default=none)>\n";
	print "      -h <show this help>\n";
    print "\n";
    exit;
}
# ==============================================================================

