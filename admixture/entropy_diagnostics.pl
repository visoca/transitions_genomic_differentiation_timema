#!/usr/bin/env perl

# (c) Victor Soria-Carrasco
# victor.soria.carrasco@gmail.com
# Last modified: 10/03/2014 17:13:58

# Description:
# This script plot traces for deviance and other parameters
# estimated by entropy to check for convergence and mixing
# entropy output files expected are:
# *_k2_1.hdf5, *_k2_2.hdf5, *_k3_1.hdf5, *_k3_2.hdf5 etc

# WARNING: Beware this script may need large amounts of memory to run (R code)

use warnings;
use strict;
use Getopt::Long;
use File::Basename;
use File::Path qw(make_path);
use File::Spec;
use Statistics::R;

my $estpost='estpost';

my $version='1.0-2014.03.10';
my $ncpu=1;

&author;
my $mink=2;
my $maxk=3;
my $burnin=0;
my ($indir, $outdir);
GetOptions(
    'i|I=s'  => \$indir,
    'o|O=s'  => \$outdir,
	'mink=i' => \$mink,
	'maxk=i' => \$maxk,
	'b=i'    => \$burnin,
    'h|help' => \&usage
);

&usage if (!defined($indir) || !defined($outdir));

my $nks=$maxk-$mink+1;

# Input directory
if (! -e $indir){
	die ("\nCan't access input file: $indir\n\n");
}
$indir=File::Spec->rel2abs($indir);

# Output directory
if (! -e $outdir){
	eval {make_path($outdir)}
		or die ("\nCan't create output directory: $outdir\n\n");
}
$outdir=File::Spec->rel2abs($outdir);

opendir (DIR, $indir)
	or die("\nan't open input directory: $indir\n\n");
	my @hdf5files=grep(/\.hdf5$/, readdir(DIR));
	foreach my $h (@hdf5files) {
		$h="$indir/$h";
	}
closedir (DIR);

my @hdf5run1=grep(/\_1.hdf5$/, @hdf5files);
my $nruns=0;

$nruns=scalar(@hdf5files)/scalar(@hdf5run1) if (scalar(@hdf5run1));

my $R = Statistics::R->new();
foreach my $k ($mink..$maxk){
	print "Processing files for K=$k...\n\n";

	my @hdf5=grep(/.*\_k$k(\_[0-9]+){0,1}\.hdf5$/, sort @hdf5files);
	my $output=basename($hdf5[0]);
	$output=~ s/(\_[0-9]+){0,1}\.hdf5/\.deviance\.txt/g;
	$output=~ s/\.deviance\.txt/\.b$burnin\.deviance\.txt/g if ($burnin > 0);
	$output="$outdir/$output";
	system("echo -n > $output");
	foreach my $h (@hdf5){
		print "\t$h\n";
		system("$estpost -b $burnin -p deviance -s 2 -o $output.tmp $h >& $output.log");
		system("cat $output.tmp | sed 's/deviance\,//g' >> $output");
		unlink glob ("$output.tmp")
	}
	my $png=basename($output);
	$png=~ s/\.txt/\.png/g;
	$png="$outdir/$png";
	plot_deviance($output,$png,$k);
	print "\n\tPlot saved as $png\n\n";
	unlink glob ("$output");
}
$R->stop();


# ==============================================================================
# ==============================================================================
# ============================== SUBROUTINES ===================================
# ==============================================================================
# ==============================================================================

# Show copyright
# ==============================================================================
sub author{
    print "\n";
    print "#########################################\n";
    print "  ".basename($0)."\n";
	print "  version $version\n";
    print "  (c) Victor Soria-Carrasco             \n";
    print "  victor.soria.carrasco\@gmail.com      \n";
    print "#########################################\n";
	print "\n";
}
# ==============================================================================

# Show usage
# ==============================================================================
sub usage{
    print "\n";
	print "  Usage:\n";
    print "    ".basename($0)."\n";
	print "      -i <input directory (with *_k[0-9]+_[0-9]+.hdf5 files)>\n";
	print "      -o <output directory>\n";
	print "      -mink <max. K (optional, default=2)>\n";
	print "      -maxk <max. K (optional, default=3)>\n";
	print "      -b <extra burnin (optional, default=0)>\n";
	print "      -h <show this help>\n";
    print "\n";
    exit;
}
# ==============================================================================

# Plot trace plot for deviance
# ==============================================================================
sub plot_deviance{
	my $devfile=shift;
	my $output=shift;
	my $k=shift;

	$R->run(qq`
		# deviance<-as.numeric(scan("$devfile", what="character", sep=",")[-1])
		deviance<-scan("$devfile", sep=",")
		png(filename="$output", width=1200, height=400, res=100)
		plot(seq(length(deviance)), deviance, type="l",
			xlab="MCMC samples", ylab="deviance")
		for (k in 2:$k) abline (v=((length(deviance)/$nruns)*(k-1)), lty=3, lwd=2, col="grey")
		abline(h=mean(deviance),col="red")
		abline(h=quantile(deviance, c(0.025,0.975)), col="red", lty=3)
		dev.off()`);
}
# ==============================================================================
